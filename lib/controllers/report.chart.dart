import 'package:flutter/cupertino.dart';

import '../data/local/product.model.dart';
import '../data/local/receipt.model.dart';
import '../data/local/receipt_line.model.dart';
import '../data/local/stock_movement.model.dart';
import '../ui/screens/report.dart';

class ReportController extends ChangeNotifier {
  ReceiptEloquent eloquent = ReceiptEloquent();
  ReceiptLineEloquent receiptLineEloquent = ReceiptLineEloquent();
  ProductEloquent productDataSource = ProductEloquent();
  ReceiptLineEloquent receiptLineDataSource = ReceiptLineEloquent();
  StockMovementEloquent stockMovementDataSource = StockMovementEloquent();

  late List<ProductModel> _products = [];
  late List<StockMovementModel> _stockMovements = [];

  List<ProductModel> get products => _products;

  List<StockMovementModel> get stockMovements => _stockMovements;

  //receipts
  final List<Receipt> _receipts = [];

  List<Receipt> get receipts => _receipts;

  //receipts lines
  final List<ReceiptLineModel> _receiptLines = [];

  List<ReceiptLineModel> get receiptLines => _receiptLines;

  //total sales is total receipts amount in base currency
  double get totalSales => _receipts.map((e) => (double.parse(e.totalAmount) / e.rate)).fold(0, (a, b) => a + b);

  //profit is total profit in base currency is the total profits in receipt lines
  double get profit => _receiptLines.map((e) => e.profit).fold(0, (a, b) => a + b);

  //inventory loss is the total stock out where not sales
  double get inventoryLoss => _stockMovements
      .where((element) => (element.stockMovement == 'stockOut' && element.entryType != 'SALES'))
      .map((e) => e.quantity * (e.buyingPrice / double.parse(e.rate)))
      .fold(0, (a, b) => a + b);

  //inventory gain is the total stock in where not stock out
  double get inventoryGain => _stockMovements
      .where((element) => (element.stockMovement == 'stockIn'))
      .map((e) => e.quantity * (e.buyingPrice / double.parse(e.rate)))
      .fold(0, (a, b) => a + b);

  //total stock value is the total stock value is opening stock + inventory gain - inventory loss
  double get stockValue =>
      _products.map((e) => (double.parse(e.quantity) * (double.parse(e.purchasePrice) / e.rate))).fold(0, (a, b) => a + b);

  //total stock value is the total stock value is opening stock + inventory gain - inventory loss
  double get weightedAverageCost => stockValue - inventoryLoss + inventoryGain;

  //the report activity
  List<SalesTile> get reportActivity => _receiptLines
      .map((elem) => SalesTile(
            productName: elem.productName,
            price: elem.price,
            quantity: elem.quantity,
            currency: elem.currency.split(' | ')[1] == 'ZIG' ? 'ZWL' : elem.currency.split(' | ')[1],
            rate: elem.rate,
            margin: elem.margin,
            sold: elem.totalAmount,
            stockValue: '0',
          ))
      .toList();

  //initialize report session
  initializeReportSession() async {
    List<Map<String, dynamic>> data = await eloquent.all();
    _receipts.clear();
    for (var item in data) {
      _receipts.add(Receipt.fromJson(item));
    }

    List<Map<String, dynamic>> lines = await receiptLineEloquent.all();
    _receiptLines.clear();
    for (var item in lines) {
      _receiptLines.add(ReceiptLineModel.fromJson(item));
    }

    var result = await stockMovementDataSource.all();
    _stockMovements = result.map((json) => StockMovementModel.fromJson(json)).toList();

    var reso = await productDataSource.all();
    _products = reso.map((json) => ProductModel.fromJson(json)).toList();

    notifyListeners();
  }
}

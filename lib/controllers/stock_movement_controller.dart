import 'package:flutter/cupertino.dart';
import 'package:retail/data/local/stock_movement.model.dart';

class StockMovementController extends ChangeNotifier {
  StockMovementEloquent stockMovementDataSource = StockMovementEloquent();

  StockMovementController() {
    stockMovementDataSource.createTable();
  }

  //add stock movement
  void addStockMovement(StockMovementModel stockMovement) {
    stockMovementDataSource.create(stockMovement.toJson());
    notifyListeners();
  }
}

import 'package:flutter/cupertino.dart';
import 'package:retail/controllers/stock_movement_controller.dart';
import '../data/local/product.model.dart';
import '../data/local/receipt.model.dart';
import '../data/local/receipt_line.model.dart';
import '../data/local/stock_movement.model.dart';
import 'cart.dart';

class ReceiptController extends ChangeNotifier {
  ReceiptEloquent eloquent = ReceiptEloquent();
  ReceiptLineEloquent receiptLineEloquent = ReceiptLineEloquent();
  ProductEloquent productDataSource = ProductEloquent();
  ReceiptLineEloquent receiptLineDataSource = ReceiptLineEloquent();

  //receipts
  final List<Receipt> _receipts = [];

  List<Receipt> get receipts => _receipts;

  //receipts lines
  final List<ReceiptLineModel> _receiptLines = [];

  List<ReceiptLineModel> get receiptLines => _receiptLines;

  //products
  late List<ProductModel> _products = [];

  List<ProductModel> get products => _products;

  ReceiptController() {
    eloquent.createTable();
    receiptLineEloquent.createTable();
  }

  Future<void> createReceipt(Receipt receipt, List<CartModel> cart) async {
    int id = await eloquent.create(receipt.toJson());

    print('Receipt Created $id');

    print('Cart Length ${cart.length}');

    for (CartModel item in cart) {
      print('Item ${item.toString()}');

      // ProductModel product = _products.firstWhere((element) => element.primaryKey == item.product.primaryKey);
      double profit = (double.parse(item.product.price) - double.parse(item.product.purchasePrice)) * item.quantityProduct;
      double margin = (profit / (double.parse(item.product.price) * item.quantityProduct)) * 100;

      var line = ReceiptLineModel(
        productName: item.product.productName,
        productCode: item.product.productCode,
        productId: item.product.primaryKey!,
        receiptId: id,
        currency: receipt.currencyName,
        currencyCode: receipt.currencyCode,
        rate: receipt.rate,
        price: double.parse(item.product.price),
        quantity: item.quantityProduct.toDouble(),
        totalAmount: double.parse(item.product.price) * item.quantityProduct,
        margin: margin,
        profit: profit,
        createdAt: DateTime.now().toString(),
        updatedAt: DateTime.now().toString(),
      ).toJson();

      print('Receipt Line Created ${line.toString()}');
      receiptLineEloquent.create(line);

      StockMovementModel stockMovement = StockMovementModel(
        stockMovement: 'stockOut',
        date: DateTime.now().toString(),
        product: item.product.productName,
        productId: item.product.primaryKey!,
        currency: receipt.currencyCode,
        rate: receipt.rate.toString(),
        quantity: item.quantityProduct.toDouble(),
        buyingPrice: double.parse(item.product.purchasePrice),
        sellingPrice: double.parse(item.product.price),
        createdAt: DateTime.now().toString(),
        updatedAt: DateTime.now().toString(),
        description: 'Sale Of ${item.product.productName}',
        entryType: 'SALES',
        actualPriceCOGS: double.parse(item.product.purchasePrice) * item.quantityProduct,
      );

      StockMovementController().addStockMovement(stockMovement);
    }
  }

  initReceipts() async {
    var result = await productDataSource.all();
    _products = result.map((json) => ProductModel.fromJson(json)).toList();

    List<Map<String, dynamic>> data = await eloquent.all();
    _receipts.clear();
    for (var item in data) {
      _receipts.add(Receipt.fromJson(item));
    }

    List<Map<String, dynamic>> lines = await receiptLineEloquent.all();
    _receiptLines.clear();
    for (var item in lines) {
      _receiptLines.add(ReceiptLineModel.fromJson(item));
    }

    print(_receiptLines.length);
    notifyListeners();
  }
}

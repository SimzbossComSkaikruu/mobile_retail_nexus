import 'package:flutter/cupertino.dart';
import 'package:retail/data/local/product.model.dart';
import 'package:retail/data/local/stock_movement.model.dart';
import '../data/local/prices.model.dart';


class ProductController extends ChangeNotifier {
  ValueNotifier<int> refreshNotifier = ValueNotifier(0);
  ProductEloquent productDataSource = ProductEloquent();
  StockMovementEloquent stockMovementDataSource = StockMovementEloquent();
  PriceEloquent priceDataSource = PriceEloquent();

  late List<ProductModel> _products = [];
  late List<StockMovementModel> _stockMovements = [];
  late List<StockMovementModel> _stocks = [];

  ProductController() {
    productDataSource.createTable();
    priceDataSource.createTable();
  }

  List<ProductModel> get products => _products;

  List<StockMovementModel> get stockMovements => _stockMovements;

  List<StockMovementModel> get stocksOnProduct => _stocks;

  Future<void> initialize() async {
    await getProducts();
    await getStockMovements();
    notifyListeners();
  }

  //add product
  void addProduct(ProductModel product) {
    productDataSource.create(product.toJson());
    //
    // PricesModel pricesModel = PricesModel(
    //   productName: ,
    //   productCode: productCode,
    //   productId: productId,
    //   price: double.parse(price),
    //   currency: 'USD',
    //   rate: 1.00,
    // );
    //
    // createProductSale(pricesModel);

    refreshNotifier.value++;
    notifyListeners();
  }

  //get all products
  //retrieve all products
  Future<void> getProducts() async {
    var result = await productDataSource.all();
    _products = result.map((json) => ProductModel.fromJson(json)).toList();
    notifyListeners();
  }

  //get all stock movements
  Future<void> getStockMovements() async {
    var result = await stockMovementDataSource.all();
    _stockMovements = result.map((json) => StockMovementModel.fromJson(json)).toList();
    notifyListeners();
  }

  //get stock per product
  getStockValue(int productId) {
    _stocks = _stockMovements.where((element) => element.productId == productId).toList();
  }

  find(productId) async {
    var productM = await productDataSource.find(productId);
    return ProductModel.fromJson(productM!);
  }

  //update price
  void updatePrice(int productId, String price, String productName, String productCode) async {
    productDataSource.updateById(productId, {'price': price});

    var product = await productDataSource.find(productId);
    print(product);

    PricesModel pricesModel = PricesModel(
      productName: productName,
      productCode: productCode,
      productId: productId,
      price: double.parse(price),
      currency: 'USD',
      rate: 1.00,
    );

    createProductSale(pricesModel);
    print('Price updated');

    refreshNotifier.value++;

    notifyListeners();
  }

  //creat a sale
  void createProductSale(PricesModel pricesModel) {
    priceDataSource.create(pricesModel.toJson());
    notifyListeners();
  }
}

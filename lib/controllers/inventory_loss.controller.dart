import 'package:flutter/cupertino.dart';
import 'package:retail/controllers/product_controller.dart';
import 'package:retail/controllers/stock_movement_controller.dart';

import '../data/local/product.model.dart';
import '../data/local/product_loss.model.dart';
import '../data/local/stock_movement.model.dart';

class InventoryLossController extends ChangeNotifier {
  ProductLossEloquent eloquent = ProductLossEloquent();

  InventoryLossController() {
    eloquent.createTable();
  }

  Future<void> addInventoryLoss(ProductLossModel productLoss) async {
    // Add product loss
    eloquent.create(productLoss.toJson());

    ProductController productController = ProductController();
    productController.getStockMovements();

    //calculate the average cost price
    ProductModel product = await productController.find(productLoss.productId);

    var stocks = productController.stockMovements
        .where((element) => element.productId == product.primaryKey)
        .toList();

    double inStockTotalValue = stocks
        .where((element) => element.stockMovement == 'stockIn')
        .map((e) => (e.quantity * (e.buyingPrice / double.parse(e.rate))))
        .fold(0, (a, b) => a + b);

    double outStockTotalValue = stocks
        .where((element) => element.stockMovement == 'stockOut')
        .map((e) => (e.quantity * (e.buyingPrice / double.parse(e.rate))))
        .fold(0, (a, b) => a + b);

    double inStock = stocks
        .where((element) => element.stockMovement == 'stockIn')
        .map((e) => e.quantity)
        .fold(0, (a, b) => a + b);
    double outStock = stocks
        .where((element) => element.stockMovement == 'stockOut')
        .map((e) => e.quantity)
        .fold(0, (a, b) => a + b);

    double currentStock = (double.parse(product.quantity)) + inStock - outStock;
    double stockValue = (double.parse(product.quantity) *
            (double.parse(product.purchasePrice) / product.rate)) +
        inStockTotalValue -
        outStockTotalValue;

    //average price
    double averagePrice = stockValue / currentStock;

    // Create stock movement model
    StockMovementModel stockMovement = StockMovementModel(
      stockMovement: 'stockOut',
      date: productLoss.dateCapture,
      product: productLoss.product,
      productId: productLoss.productId,
      currency: 'USD',
      rate: '1',
      quantity: productLoss.quantity,
      buyingPrice: averagePrice,
      sellingPrice: averagePrice,
      description: productLoss.description,
      entryType: 'MANUAL',
      actualPriceCOGS: productLoss.totalCost,
    );

    // Add stock movement
    StockMovementController().addStockMovement(stockMovement);
    notifyListeners();
  }
}

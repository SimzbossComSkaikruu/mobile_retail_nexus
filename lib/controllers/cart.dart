import 'package:flutter/cupertino.dart';
import '../data/local/product.model.dart';

class CartController extends ChangeNotifier {
  ValueNotifier<int> refreshNotifier = ValueNotifier(0);

  late double _totalAmount = 0.0;
  late final List<ProductModel> _cart = [];
  late int _cartCount = 0;

  late final List<CartModel> _uniqueProducts = [];

  List<CartModel> get uniqueProducts => _uniqueProducts;

  double get totalAmount => _totalAmount;

  List<ProductModel> get cart => _cart;

  int get cartCount => _cartCount;

  void addToCart(ProductModel product) {
    _cart.add(product);
    _cartCount++;
    _totalAmount = _totalAmount + double.parse(product.price);

    var quantity = [..._cart.where((element) => element.primaryKey == product.primaryKey)].length;

    if (quantity <= 1) {
      _uniqueProducts.add(CartModel(product: product, quantityProduct: 1));
    } else {
      var index = _uniqueProducts.indexWhere((element) => element.product.primaryKey == product.primaryKey);
      _uniqueProducts[index] = CartModel(product: product, quantityProduct: quantity);
    }

    refreshNotifier.value++;
    notifyListeners();
    print('Item added to cart $_cartCount $_totalAmount');
  }

  void removeFromCart(ProductModel product) {
    _cart.remove(product);
    _cartCount--;
    _totalAmount = _totalAmount - double.parse(product.price);
    var quantity = [..._cart.where((element) => element.primaryKey == product.primaryKey)].length;
    if (quantity <= 0) {
      _uniqueProducts.removeWhere((element) => element.product.primaryKey == product.primaryKey);
    } else {
      var index = _uniqueProducts.indexWhere((element) => element.product.primaryKey == product.primaryKey);
      _uniqueProducts[index] = CartModel(product: product, quantityProduct: quantity);
    }

    refreshNotifier.value++;
    notifyListeners();
    print('Item removed from cart');
  }

  void removeProduct(ProductModel product) {
    var items = _cart.where((element) => element.primaryKey == product.primaryKey).toList();
    _cart.removeWhere((element) => element.primaryKey == product.primaryKey);
    _cartCount = _cartCount - items.length;
    _totalAmount = _totalAmount - (double.parse(product.price) * items.length);
    _uniqueProducts.removeWhere((element) => element.product.primaryKey == product.primaryKey);

    refreshNotifier.value++;
    notifyListeners();
    print('Item removed from cart');
  }

  void checkOut() {
    print('Checking out');
  }

  void clearCart() {
    _cart.clear();
    _cartCount = 0;
    _totalAmount = 0.0;
    _uniqueProducts.clear();

    refreshNotifier.value++;
    notifyListeners();
    print('Cart cleared');
  }

  void updateCart() {
    refreshNotifier.value++;
    notifyListeners();
  }
}

class CartModel {
  final ProductModel product;
  final int quantityProduct;

  CartModel({required this.product, required this.quantityProduct});

  @override
  String toString() {
    return 'Product: ${product.productName} Quantity: $quantityProduct';
  }
}

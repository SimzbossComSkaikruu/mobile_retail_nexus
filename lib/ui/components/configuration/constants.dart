import 'package:flutter/material.dart';

const String appName = 'Retail App';
const String appVersion = '1.0.0';
const String appDescription = 'Retail Decision Nexus Application';
const String appDeveloper = 'Simbarashe Makoni';
const String appDeveloperEmail = 'simbapmakoni@gmail.com';

//theme constants
ThemeData themeData = ThemeData.dark().copyWith(
  scaffoldBackgroundColor: Colors.white,
  primaryColor: const Color.fromRGBO(21, 30, 61, 1),
  hintColor: const Color.fromRGBO(21, 30, 61, 1),
  focusColor: const Color.fromRGBO(21, 30, 61, 1),
);

//shimmer constants
//white color shimmer constants
const shimmerWhiteGradient = LinearGradient(
  colors: [
    Color(0xFFEBEBF4),
    Color(0xFFF4F4F4),
    Color(0xFFEBEBF4),
  ],
  stops: [
    0.1,
    0.3,
    0.4,
  ],
  begin: Alignment(-1.0, -0.3),
  end: Alignment(1.0, 0.3),
  tileMode: TileMode.clamp,
);

//font size constant
const TextStyle blackSmallBold = TextStyle(
  fontWeight: FontWeight.w900,
  fontSize: 20,
  color: Colors.black,
);

const TextStyle redSmallBold = TextStyle(
  fontWeight: FontWeight.w900,
  fontSize: 20,
  color: Colors.red,
);

const TextStyle blackSmall = TextStyle(
  fontWeight: FontWeight.w300,
  color: Colors.black,
);

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../controllers/report.chart.dart';
import '../drawer/drawer_widget_menu.dart';
import 'inventory/add_inventory.dart';

class InventoryStatusReport extends StatefulWidget {
  const InventoryStatusReport({super.key, required this.openDrawer});

  final VoidCallback openDrawer;

  @override
  State<InventoryStatusReport> createState() => _InventoryStatusReportState();
}

class _InventoryStatusReportState extends State<InventoryStatusReport> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        leading: DrawerWidgetMenu(onClicked: widget.openDrawer),
        title: const Text(
          'Financial Status',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 30.0),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.share),
            onPressed: () {},
          ),
        ],
      ),
      body: FutureBuilder(
        future: Provider.of<ReportController>(context, listen: false).initializeReportSession(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return snapshot.connectionState == ConnectionState.waiting
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : Consumer<ReportController>(
                  builder: (context, productController, child) {
                    return SingleChildScrollView(
                      child: Card(
                        color: Colors.white,
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width, // Full width
                          height: MediaQuery.of(context).size.height, // Full height
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    const Text(
                                      'Opening Stock Value',
                                      style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.grey),
                                    ),
                                    Text(
                                      'USD ${productController.stockValue.toStringAsFixed(2)}',
                                      style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700, color: Colors.blue),
                                    ),
                                  ],
                                ),
                                const Center(
                                  child: Divider(
                                    height: 10,
                                    thickness: 0.8,
                                    indent: 1,
                                    endIndent: 1,
                                    color: Colors.grey,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    const Text(
                                      'Total Sales',
                                      style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.grey),
                                    ),
                                    Text(
                                      'USD ${productController.totalSales.toStringAsFixed(2)}',
                                      style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700, color: Colors.blue),
                                    ),
                                  ],
                                ),
                                const Center(
                                  child: Divider(
                                    height: 10,
                                    thickness: 0.8,
                                    indent: 1,
                                    endIndent: 1,
                                    color: Colors.grey,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    const Text(
                                      'Profit',
                                      style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.grey),
                                    ),
                                    Text(
                                      'USD ${productController.profit.toStringAsFixed(2)}',
                                      style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700, color: Colors.blue),
                                    ),
                                  ],
                                ),
                                const Center(
                                  child: Divider(
                                    height: 10,
                                    thickness: 0.8,
                                    indent: 1,
                                    endIndent: 1,
                                    color: Colors.grey,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    const Text(
                                      'Inventory Loss',
                                      style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.grey),
                                    ),
                                    Text(
                                      'USD ${productController.inventoryLoss.toStringAsFixed(2)}',
                                      style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700, color: Colors.blue),
                                    ),
                                  ],
                                ),
                                const Center(
                                  child: Divider(
                                    height: 10,
                                    thickness: 0.8,
                                    indent: 1,
                                    endIndent: 1,
                                    color: Colors.grey,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    const Text(
                                      'Inventory Gain',
                                      style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.grey),
                                    ),
                                    Text(
                                      'USD ${productController.inventoryGain.toStringAsFixed(2)}',
                                      style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700, color: Colors.blue),
                                    ),
                                  ],
                                ),
                                const Center(
                                  child: Divider(
                                    height: 10,
                                    thickness: 0.8,
                                    indent: 1,
                                    endIndent: 1,
                                    color: Colors.grey,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    const Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Total Stock Value',
                                          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.grey),
                                        ),
                                        Text(
                                          '(Weighted Average Cost)',
                                          style: TextStyle(fontSize: 10.0, fontWeight: FontWeight.w700, color: Colors.red),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      'USD ${productController.weightedAverageCost.toStringAsFixed(2)}',
                                      style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700, color: Colors.blue),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                const Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Product',
                                      style: TextStyle(fontSize: 10.0, fontWeight: FontWeight.w500, color: Colors.blue),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Text(
                                      'Price',
                                      style: TextStyle(fontSize: 10.0, fontWeight: FontWeight.w500, color: Colors.blue),
                                    ),
                                    Text(
                                      'Sold',
                                      style: TextStyle(fontSize: 10.0, fontWeight: FontWeight.w500, color: Colors.blue),
                                    ),
                                    Text(
                                      'Margin',
                                      style: TextStyle(fontSize: 10.0, fontWeight: FontWeight.w500, color: Colors.blue),
                                    ),
                                  ],
                                ),
                                const Center(
                                  child: Divider(
                                    height: 10,
                                    thickness: 0.8,
                                    indent: 1,
                                    endIndent: 1,
                                    color: Colors.grey,
                                  ),
                                ),
                                ...productController.reportActivity
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                );
        },
      ),
    );
  }
}

class SalesTile extends StatelessWidget {
  const SalesTile(
      {super.key,
      required this.productName,
      required this.price,
      required this.quantity,
      required this.currency,
      required this.rate,
      required this.margin,
      required this.sold,
      required this.stockValue});

  //product name
  final String productName;

  //product price
  final double price;

  //product quantity
  final double quantity;

  //product currency
  final String currency;

  //rate used
  final double rate;

  //product margin
  final double margin;

  //product sold
  final double sold;

  //product stock value
  final String stockValue;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          width: 80,
          child: Text(
            '$productName x $quantity',
            style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.grey),
          ),
        ),
        const SizedBox(
          width: 20,
        ),
        Text(
          '${price.toString()} ${CurrencyUtil().currencyEmoji('usd')}',
          style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w200, color: Colors.black),
        ),
        Text(
          '$sold x $rate ${CurrencyUtil().currencyEmoji(currency)}',
          style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w200, color: Colors.black),
        ),
        Text(
          '$margin %',
          style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w200, color: Colors.black),
        ),
      ],
    );
  }
}

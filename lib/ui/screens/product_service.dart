import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import '../../controllers/product_controller.dart';
import '../drawer/drawer_widget_menu.dart';
import 'add_product.dart';

class ProductServiceScreen extends StatefulWidget {
  final VoidCallback openDrawer;

  const ProductServiceScreen({super.key, required this.openDrawer});

  @override
  State<ProductServiceScreen> createState() => _ProductServiceScreenState();
}

class _ProductServiceScreenState extends State<ProductServiceScreen> {
  @override
  void initState() {
    super.initState();
    Provider.of<ProductController>(context, listen: false).refreshNotifier;
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: Provider.of<ProductController>(context, listen: false).refreshNotifier,
        builder: (context, value, child) {
          return FutureBuilder(
            future: Provider.of<ProductController>(context, listen: false).getProducts(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              return snapshot.connectionState == ConnectionState.waiting
                  ? const Scaffold(
                      body: SafeArea(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    )
                  : Consumer<ProductController>(
                      builder: (context, productController, child) {
                        final screenSize = MediaQuery.of(context).size.height;
                        return Scaffold(
                          backgroundColor: Colors.blue,
                          appBar: AppBar(
                            elevation: 2.0,
                            backgroundColor: Colors.white,
                            leading: DrawerWidgetMenu(onClicked: widget.openDrawer),
                            title: const Text(
                              'Products',
                              style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 30.0),
                            ),
                          ),
                          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
                          floatingActionButton: _getFloatingActionButton(screenSize),
                          body: ListView.builder(
                            itemCount: productController.products.length,
                            itemBuilder: (context, index) {
                              return Card(
                                color: Colors.white,
                                child: ListTile(
                                  leading: const Icon(
                                    Icons.shopping_bag_rounded,
                                    color: Colors.indigoAccent,
                                  ),
                                  title: Text('Product : ${productController.products[index].productName}'),
                                  subtitle: Text('Category ${productController.products[index].productCategory}'),
                                  trailing: IconButton(
                                    color: Colors.red,
                                    icon: const Icon(Icons.delete),
                                    onPressed: () {},
                                  ),
                                ),
                              );
                            },
                          ),
                        );
                      },
                    );
            },
          );
        });
  }

  Widget _getFloatingActionButton(height) => FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const AddProductScreen()),
          );
        },
        backgroundColor: Colors.white,
        child: const Icon(FontAwesomeIcons.plus, color: Colors.black),
      );
}

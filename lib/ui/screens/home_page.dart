import 'package:chart_sparkline/chart_sparkline.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:retail/ui/screens/pos/charts/sales.predictions.dart';
import 'package:retail/ui/screens/pos/sales.pre.trends.dart';

import '../drawer/drawer_widget_menu.dart';
import '../widgets/custom_widgets.dart';

class HomePage extends StatefulWidget {
  final VoidCallback openDrawer;

  const HomePage({super.key, required this.openDrawer});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //charts graph data

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        leading: DrawerWidgetMenu(onClicked: widget.openDrawer),
        title: const Text(
          'Dashboard',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 30.0),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.fromLTRB(5, 10, 5, 0),
        child: SingleChildScrollView(
          child: StaggeredGrid.count(
            crossAxisCount: 4,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            children: <Widget>[
              StaggeredGridTile.count(
                crossAxisCellCount: 4,
                mainAxisCellCount: 1.5,
                child: WidgetsCustom(
                  child: Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        const Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('Today Sale', style: TextStyle(color: Colors.blueAccent)),
                            Text(
                              '',
                              style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 30.0),
                            )
                          ],
                        ),
                        Material(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(20.0),
                          child: const Center(
                            child: Padding(
                              padding: EdgeInsets.all(16.0),
                              child: Icon(Icons.timeline, color: Colors.white, size: 25.0),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              const StaggeredGridTile.count(
                crossAxisCellCount: 2,
                mainAxisCellCount: 2,
                child: WidgetsCustom(
                  child: Padding(
                    padding: EdgeInsets.all(24.0),
                    child:
                        Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                      Material(
                          color: Colors.teal,
                          shape: CircleBorder(),
                          child: Padding(
                            padding: EdgeInsets.all(16.0),
                            child: Icon(Icons.plus_one_rounded, color: Colors.white, size: 25.0),
                          )),
                      Padding(
                        padding: EdgeInsets.only(bottom: 16.0),
                      ),
                      Text('New Sale', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 20.0)),
                      Text(
                        'Create New Sale',
                        style: TextStyle(color: Colors.black45),
                      ),
                    ]),
                  ),
                  // onTap: () {},
                ),
              ),
              const StaggeredGridTile.count(
                crossAxisCellCount: 2,
                mainAxisCellCount: 2,
                child: WidgetsCustom(
                  child: Padding(
                    padding: EdgeInsets.all(24.0),
                    child:
                        Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
                      Material(
                          color: Colors.amber,
                          shape: CircleBorder(),
                          child: Padding(
                            padding: EdgeInsets.all(16.0),
                            child: Icon(Icons.notifications, color: Colors.white, size: 25.0),
                          )),
                      Padding(
                        padding: EdgeInsets.only(bottom: 16.0),
                      ),
                      Text('Alerts', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 20.0)),
                      Text(
                        'Notifications',
                        style: TextStyle(color: Colors.black45),
                      ),
                    ]),
                  ),
                  // onTap: () {},
                ),
              ),
              StaggeredGridTile.count(
                crossAxisCellCount: 4,
                mainAxisCellCount: 3,
                child: InkWell(
                    onTap: () => _pushScreen(
                          context: context,
                          screen: const InventoryTrends(),
                        ),
                    child: const SalesPrediction()),
              ),
              StaggeredGridTile.count(
                crossAxisCellCount: 4,
                mainAxisCellCount: 1.5,
                child: WidgetsCustom(
                  child: Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          const Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('Total Customers Recorded', style: TextStyle(color: Colors.redAccent)),
                              Text('0', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 34.0))
                            ],
                          ),
                          Material(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(24.0),
                            child: const Center(
                              child: Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Icon(Icons.store, color: Colors.white, size: 30.0),
                              ),
                            ),
                          )
                        ]),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

void _pushScreen({required BuildContext context, required Widget screen}) {
  ThemeData themeData = Theme.of(context);
  Navigator.push(
    context,
    MaterialPageRoute(builder: (_) => Theme(data: themeData, child: screen)),
  );
}

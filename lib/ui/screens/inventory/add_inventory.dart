import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:retail/data/local/product.model.dart';
import 'package:retail/ui/widgets/form.dart';
import '../../../controllers/product_controller.dart';
import '../../../controllers/stock_movement_controller.dart';
import '../../../data/local/stock_movement.model.dart';
import '../../drawer/drawer_widget_menu.dart';
import '../../widgets/date_input.dart';
import '../../widgets/dropdown.dart';
import '../../widgets/form_input.dart';

class AddInventory extends StatelessWidget {
  const AddInventory({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          elevation: 2.0,
          backgroundColor: Colors.white,
          leading:
              const DrawerWidgetMenu(icon: FaIcon(FontAwesomeIcons.arrowLeft)),
          title: const Text(
            'Add Inventory',
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w700,
                fontSize: 30.0),
          ),
        ),
        body: SafeArea(
          child: FutureBuilder(
            future: Provider.of<ProductController>(context, listen: false)
                .getProducts(),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              return snapshot.connectionState == ConnectionState.waiting
                  ? const Center(child: CircularProgressIndicator())
                  : Consumer<ProductController>(
                      builder: (context, productController, child) {
                        return Card(
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(1.0)),
                          child: UpdateInventory(
                              products: productController.products),
                        );
                      },
                    );
            },
          ),
        ),
      );
}

class UpdateInventory extends StatefulWidget {
  const UpdateInventory({super.key, required this.products});

  final List<ProductModel> products;

  @override
  State<UpdateInventory> createState() => _UpdateInventoryState();
}

class _UpdateInventoryState extends State<UpdateInventory> {
  final _formKey = GlobalKey<FormState>();
  final dateCaptureController = TextEditingController();
  final productController = TextEditingController();
  final quantityController = TextEditingController();
  final sellingPriceController = TextEditingController();
  final currencyController = TextEditingController();
  final buyingRateController = TextEditingController();
  final descriptionController = TextEditingController();
  final buyingPriceController = TextEditingController();

  //create focus node
  late FocusNode quantityFocusNode;
  late FocusNode sellingPriceFocusNode;
  late FocusNode buyingRateFocusNode;
  late FocusNode buyingPriceNode;
  late FocusNode descriptionFocusNode;

  late String selectedStockMovement = "stockIn";

  List<ProductModel> get products => widget.products;

  final int year = DateTime.now().year;
  final int month = DateTime.now().month;
  final int day = DateTime.now().day;

  @override
  void initState() {
    super.initState();
    dateCaptureController.text = '$day/$month/$year';

    //initialize focus node
    quantityFocusNode = FocusNode();
    sellingPriceFocusNode = FocusNode();
    buyingRateFocusNode = FocusNode();
    buyingPriceNode = FocusNode();
    descriptionFocusNode = FocusNode();
  }

  @override
  void dispose() {
    //dispose controllers
    dateCaptureController.dispose();
    productController.dispose();
    quantityController.dispose();
    sellingPriceController.dispose();
    currencyController.dispose();
    buyingRateController.dispose();
    descriptionController.dispose();
    buyingPriceController.dispose();

    //dispose focus node
    quantityFocusNode.dispose();
    sellingPriceFocusNode.dispose();
    buyingRateFocusNode.dispose();
    buyingPriceNode.dispose();
    descriptionFocusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return CustomForm(
      formInputs: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            DateInput(
              controller: dateCaptureController,
              width: (width / 2),
              contentPadding: 1.0,
              iconColorS: Colors.blue,
            ),
            InOutSegmentButton(
                onSelected: (String value) =>
                    setState(() => selectedStockMovement = value)),
          ],
        ),
        const Center(
          child: Divider(
            height: 10,
            thickness: 1,
            indent: 1,
            endIndent: 1,
            color: Colors.blue,
          ),
        ),
        CustomDropdownMenu(products: products, controller: productController),
        const Center(
          child: Divider(
            height: 10,
            thickness: 1,
            indent: 1,
            endIndent: 1,
            color: Colors.blue,
          ),
        ),
        CustomDropDown<IconLabel>(
          items: IconLabel.values.map<DropdownMenuEntry<IconLabel>>(
                (IconLabel icon) => DropdownMenuEntry<IconLabel>(
                  style: ButtonStyle(
                    textStyle: MaterialStateProperty.all<TextStyle>(
                      const TextStyle(
                          fontStyle: FontStyle.normal,
                          color: Colors.black,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                  value: icon,
                  label: icon.label,
                  leadingIcon: Text(
                    CurrencyUtil().currencyEmoji(icon.icon),
                    style: const TextStyle(
                        fontStyle: FontStyle.normal,
                        color: Colors.black,
                        fontWeight: FontWeight.w300),
                  ),
                ),
              )
              .toList(),
          controller: currencyController,
          label: const Text("Select Currency"),
          leadingIcon: const Icon(FontAwesomeIcons.moneyCheckDollar),
        ),
        InputForm(
          label: 'Buying Rate Against 1 USD',
          prefix: const Icon(FontAwesomeIcons.dollarSign),
          labelStyle: const TextStyle(
              fontStyle: FontStyle.normal,
              color: Colors.black,
              fontWeight: FontWeight.w300),
          focusNode: buyingRateFocusNode,
          inputType: TextInputType.number,
          controller: buyingRateController,
          contentPadding: 1.0,
          border: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 1.0),
            borderRadius: BorderRadius.all(Radius.circular(1.0)),
            gapPadding: 2.0,
          ),
        ),
        InputForm(
          label: 'Quantity',
          prefix: const Icon(FontAwesomeIcons.fileInvoice),
          labelStyle: const TextStyle(
              fontStyle: FontStyle.normal,
              color: Colors.black,
              fontWeight: FontWeight.w300),
          focusNode: quantityFocusNode,
          inputType: TextInputType.number,
          controller: quantityController,
          contentPadding: 1.0,
          border: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 1.0),
            borderRadius: BorderRadius.all(Radius.circular(1.0)),
            gapPadding: 2.0,
          ),
        ),
        InputForm(
          label: 'Buying Price Per Item',
          labelStyle: const TextStyle(
              fontStyle: FontStyle.normal,
              color: Colors.black,
              fontWeight: FontWeight.w300,),
          prefix: const Icon(FontAwesomeIcons.buyNLarge),
          inputType: TextInputType.number,
          focusNode: buyingPriceNode,
          controller: buyingPriceController,
          contentPadding: 1.0,
          border: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 1.0),
            borderRadius: BorderRadius.all(Radius.circular(1.0)),
            gapPadding: 2.0,
          ),
        ),
        InputForm(
          label: 'Selling Price Per Item',
          labelStyle: const TextStyle(
              fontStyle: FontStyle.normal,
              color: Colors.black,
              fontWeight: FontWeight.w300),
          prefix: const Icon(FontAwesomeIcons.sellcast),
          inputType: TextInputType.number,
          focusNode: sellingPriceFocusNode,
          controller: sellingPriceController,
          contentPadding: 1.0,
          border: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 1.0),
            borderRadius: BorderRadius.all(Radius.circular(1.0)),
            gapPadding: 2.0,
          ),
        ),
        InputForm(
          hintText: 'Description of Stock Movement',
          hintStyle: const TextStyle(
              fontStyle: FontStyle.normal,
              color: Colors.black,
              fontWeight: FontWeight.w300),
          hintTextDirection: TextDirection.ltr,
          maxLines: 4,
          labelStyle: const TextStyle(
              fontStyle: FontStyle.normal,
              color: Colors.black,
              fontWeight: FontWeight.w300),
          inputType: TextInputType.multiline,
          focusNode: descriptionFocusNode,
          controller: descriptionController,
          contentPadding: 2.0,
          border: const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black, width: 1.0),
            borderRadius: BorderRadius.all(Radius.circular(1.0)),
            gapPadding: 2.0,
          ),
        ),
        FloatingActionButton.extended(
          backgroundColor: Colors.blue,
          onPressed: () {
            //validate form
            if (_formKey.currentState!.validate()) {
              var date = dateCaptureController.text;
              var product = productController.text;
              var currency = currencyController.text;
              var rate = buyingRateController.text;
              var quantity = quantityController.text;
              var buyingPrice = buyingPriceController.text;
              var sellingPrice = sellingPriceController.text;
              var description = descriptionController.text;

              // print('Date: $date Product: $product Currency: $currency Rate: $rate Quantity: $quantity Buying Price: $buyingPrice Selling Price: $sellingPrice Description: $description');
              //split product to get product id
              var productId = product.split('|').first;

              StockMovementModel model = StockMovementModel(
                date: date,
                stockMovement: selectedStockMovement,
                product: product,
                currency: currency,
                rate: rate,
                quantity: double.parse(quantity),
                buyingPrice: double.parse(buyingPrice),
                sellingPrice: double.parse(sellingPrice),
                description: description,
                createdAt: DateTime.now().toString(),
                updatedAt: DateTime.now().toString(),
                productId: int.parse(productId),
                entryType: 'MANUAL',
                actualPriceCOGS:
                    (double.parse(buyingPrice) / double.parse(rate)) *
                        double.parse(quantity),
              );

              StockMovementController();
              //save to database
              //add stock movement
              Provider.of<StockMovementController>(context, listen: false)
                  .addStockMovement(model);

              //clear form
              // dateCaptureController.clear();
              productController.clear();
              currencyController.clear();
              buyingRateController.clear();
              quantityController.clear();
              buyingPriceController.clear();
              sellingPriceController.clear();
              descriptionController.clear();

              //show success message
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text('Stock Movement Added Successfully'),
                  elevation: 2.0,
                  backgroundColor: Colors.green,
                ),
              );
            }
          },
          label: const Text('Stock Movement'),
          icon: const Icon(Icons.add),
        ),
      ],
      formKey: _formKey,
    );
  }
}

enum StockMovement { stockIn, stockOut }

enum IconLabel {
  //currency flags
  usd('United States Dollar | USD', 'USA'),
  zwl('Zimbabwe Dollar | ZIG', 'ZWL'),
  zar('South African Rand | ZAR', 'ZAR'),
  bwp('Botswana Pula | BWP', 'BWP'),
  mzn('Mozambique Metical | MZN', 'MZN'),
  zmw('Zambian Kwacha | ZMW', 'ZMW'),
  gbp('British Pound | GBP', 'GBP'),
  eur('Euro | EUR', 'EUR'),
  inr('Indian Rupee | INR', 'INR'),
  aud('Australian Dollar | AUD', 'AUD'),
  nzd('New Zealand Dollar | NZD', 'NZD'),
  cad('Canadian Dollar | CAD', 'CAD');

  const IconLabel(this.label, this.icon);

  final String label;
  final String icon;
}

class InOutSegmentButton extends StatefulWidget {
  const InOutSegmentButton({super.key, required this.onSelected});

  final Function onSelected;

  @override
  State<InOutSegmentButton> createState() => _InOutSegmentButtonState();
}

class CurrencyUtil {
  currencyEmoji(String flag) {
    final String currencyFlag = flag.toUpperCase();
    final int firstLetter = currencyFlag.codeUnitAt(0) - 0x41 + 0x1F1E6;
    final int secondLetter = currencyFlag.codeUnitAt(1) - 0x41 + 0x1F1E6;
    return String.fromCharCode(firstLetter) + String.fromCharCode(secondLetter);
  }
}

class _InOutSegmentButtonState extends State<InOutSegmentButton> {
  StockMovement stockMovementView = StockMovement.stockIn;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SegmentedButton<StockMovement>(
      style: SegmentedButton.styleFrom(
        backgroundColor: Colors.grey[200],
        foregroundColor: Colors.red,
        selectedForegroundColor: Colors.white,
        selectedBackgroundColor: Colors.blue,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(1.0)),
        ),
      ),
      segments: const <ButtonSegment<StockMovement>>[
        ButtonSegment<StockMovement>(
            value: StockMovement.stockIn,
            label: Text('In'),
            icon: Icon(Icons.arrow_downward_outlined)),
        ButtonSegment<StockMovement>(
            value: StockMovement.stockOut,
            label: Text('Out'),
            icon: Icon(Icons.arrow_upward_outlined)),
      ],
      selected: <StockMovement>{stockMovementView},
      onSelectionChanged: (Set<StockMovement> newSelection) {
        widget.onSelected(newSelection.first.name);
        setState(() {
          // By default there is only a single segment that can be
          // selected at one time, so its value is always the first
          // item in the selected set.
          stockMovementView = newSelection.first;
        });
      },
    );
  }
}

class CustomDropdownMenu extends StatefulWidget {
  const CustomDropdownMenu(
      {super.key, required this.products, required this.controller});

  final TextEditingController controller;
  final List<ProductModel> products;

  @override
  State<CustomDropdownMenu> createState() => _CustomDropdownMenuState();
}

class _CustomDropdownMenuState extends State<CustomDropdownMenu> {
  String? product;

  List<ProductModel> get products => widget.products;

  TextEditingController get controller => widget.controller;

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    Widget menu = DropdownMenu<String>(
      width: (width * 0.92),
      controller: controller,
      enableFilter: true,
      requestFocusOnTap: true,
      leadingIcon: const Icon(
        FontAwesomeIcons.bagShopping,
      ),
      label: const Text('Select Product'),
      textStyle: const TextStyle(
          fontStyle: FontStyle.normal,
          color: Colors.black,
          fontWeight: FontWeight.w300),
      inputDecorationTheme: const InputDecorationTheme(
        labelStyle: TextStyle(
            fontStyle: FontStyle.normal,
            color: Colors.black,
            fontWeight: FontWeight.w300),
        contentPadding: EdgeInsets.all(1.0),
        activeIndicatorBorder: BorderSide(color: Colors.blue, width: 2.0),
        outlineBorder: BorderSide(color: Colors.blue, width: 2.0),
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue, width: 2.0),
          borderRadius: BorderRadius.all(Radius.circular(1.0)),
          gapPadding: 2.0,
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.blue, width: 2.0),
          borderRadius: BorderRadius.all(Radius.circular(1.0)),
          gapPadding: 2.0,
        ),
      ),
      onSelected: (String? icon) => setState(() => product = icon!),
      dropdownMenuEntries: products
          .map<DropdownMenuEntry<String>>(
            (ProductModel icon) => DropdownMenuEntry<String>(
              style: ButtonStyle(
                textStyle: MaterialStateProperty.all<TextStyle>(
                  const TextStyle(
                      fontStyle: FontStyle.normal,
                      color: Colors.black,
                      fontWeight: FontWeight.w300),
                ),
              ),
              value: '${icon.primaryKey}|${icon.productName}',
              label: '${icon.primaryKey}|${icon.productName}',
              leadingIcon:
                  const Icon(FontAwesomeIcons.bagShopping, color: Colors.blue),
            ),
          )
          .toList(),
    );
    return menu;
  }
}

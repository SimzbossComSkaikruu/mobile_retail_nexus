import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../../drawer/drawer_widget_menu.dart';
import '../../widgets/custom_widgets.dart';
import 'add_inventory.dart';
import 'inventory_alerts.dart';
import 'inventory_loss.dart';
import 'inventory_status.dart';
import 'inventory_reconciliation.dart';

class InventoryDashboard extends StatelessWidget {
  final VoidCallback openDrawer;

  const InventoryDashboard({super.key, required this.openDrawer});

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          elevation: 2.0,
          backgroundColor: Colors.white,
          leading: DrawerWidgetMenu(onClicked: openDrawer),
          title: const Text(
            'Inventory',
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w700,
                fontSize: 30.0),
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GridView.custom(
            gridDelegate: SliverWovenGridDelegate.count(
              crossAxisCount: 2,
              mainAxisSpacing: 4,
              crossAxisSpacing: 4,
              pattern: [
                const WovenGridTile(1),
                const WovenGridTile(
                  3 / 5,
                  crossAxisRatio: 0.9,
                  alignment: AlignmentDirectional.center,
                ),
              ],
            ),
            childrenDelegate: SliverChildBuilderDelegate(
              (context, index) => Tile(index: index),
              childCount: 4,
            ),
          ),
        ),
      );
}

class Tile extends StatelessWidget {
  final int index;
  const Tile({super.key, required this.index});

  @override
  Widget build(BuildContext context) {
    final List<String> titles = [
      'Inventory Status',
      'Add Inventory',
      // 'Inventory Reconciliation',
      'Inventory Loss',
      'Inventory Levels',
    ];
    final List<IconData> icons = [
      FontAwesomeIcons.chartBar,
      FontAwesomeIcons.store,
      // FontAwesomeIcons.readme,
      FontAwesomeIcons.lockOpen,
      FontAwesomeIcons.bell,
    ];

    final List<Color> colors = [
      Colors.blue,
      Colors.green,
      // Colors.orange,
      Colors.red,
      Colors.purple,
    ];

    final List<Widget> widgets = [
      const InventoryStatus(),
      const AddInventory(),
      // const InventoryReconciliation(),
      const InventoryLoss(),
      const InventoryAlerts(),
    ];

    return InkWell(
      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => widgets[index])),
      child: WidgetsCustom(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Material(
                    color: colors[index],
                    shape: const CircleBorder(),
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child:
                          Icon(icons[index], color: Colors.white, size: 40.0),
                    )),
                const Padding(
                  padding: EdgeInsets.only(bottom: 16.0),
                ),
                Text(titles[index],
                    style: const TextStyle(color: Colors.black)),
              ]),
        ),
      ),
    );
  }
}

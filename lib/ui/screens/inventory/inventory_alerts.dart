import 'package:chart_sparkline/chart_sparkline.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import '../../../controllers/product_controller.dart';
import '../../../data/local/stock_movement.model.dart';
import '../../drawer/drawer_widget_menu.dart';
import '../../widgets/custom_widgets.dart';

class InventoryAlerts extends StatefulWidget {
  const InventoryAlerts({super.key});

  @override
  State<InventoryAlerts> createState() => _InventoryAlertsState();
}

class _InventoryAlertsState extends State<InventoryAlerts> {
  double smoothingFactor = 0.5; // Adjust as needed
  int forecastDays = 7;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        leading:
            const DrawerWidgetMenu(icon: FaIcon(FontAwesomeIcons.arrowLeft)),
        title: const Text(
          'Inventory Levels',
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.w700, fontSize: 30.0),
        ),
      ),
      body: FutureBuilder(
        future:
            Provider.of<ProductController>(context, listen: false).initialize(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return snapshot.connectionState == ConnectionState.waiting
              ? const Center(child: CircularProgressIndicator())
              : Consumer<ProductController>(
                  builder: (context, productController, child) {
                    List<InventoryItem> inventoryData = [];

                    List<double> computeCumulativeSum(double initialValue,
                        List<StockMovementModel> inputList) {
                      final List<double> result = [initialValue];
                      double sum = initialValue;

                      // stockIn, stockOut
                      for (final StockMovementModel value in inputList) {
                        if (value.stockMovement == "stockIn") {
                          sum += value.quantity;
                        } else {
                          sum -= value.quantity;
                        }
                        result.add(sum);
                      }
                      return result;
                    }

                    for (var product in productController.products) {
                      var stocks = productController.stockMovements
                          .where((element) =>
                              element.productId == product.primaryKey)
                          .toList();
                      double inStock = stocks
                          .where(
                              (element) => element.stockMovement == 'stockIn')
                          .map((e) => e.quantity)
                          .fold(0, (a, b) => a + b);
                      double outStock = stocks
                          .where(
                              (element) => element.stockMovement == 'stockOut')
                          .map((e) => e.quantity)
                          .fold(0, (a, b) => a + b);

                      double closingStock =
                          double.parse(product.quantity) + inStock - outStock;

                      inventoryData.add(
                        InventoryItem(
                          product.productName,
                          closingStock,
                          [
                            ...computeCumulativeSum(
                                double.parse(product.quantity), [...stocks])
                          ],
                          product.measurementUnit,
                        ),
                      );
                    }

                    return ListView.builder(
                      shrinkWrap: true,
                      itemCount: inventoryData.length,
                      itemBuilder: (context, index) {
                        InventoryItem item = inventoryData[index];
                        double predictedStock = item.predictInventory(
                            smoothingFactor, forecastDays);

                        //charts graph data
                        final List<List<double>> charts = [
                          [
                            for (int i = 0; i < item.salesHistory.length; i++)
                              item.salesHistory[i],
                            predictedStock
                          ], // Last 7 days
                        ];

                        //chart dropdown items
                        final List<String> chartDropdownItems = [
                          'Next 7 days',
                          'Next 1 month',
                          // 'Last month',
                          // 'Last year',
                        ];
                        String actualDropdown = chartDropdownItems[0];
                        int actualChart = 0;

                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: WidgetsCustom(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(' ${item.name}',
                                              style: const TextStyle(
                                                  color: Colors.green)),
                                          Text(
                                            '${predictedStock.toStringAsFixed(2)} ${item.unit}',
                                            style: const TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w700,
                                                fontSize: 34.0),
                                          ),
                                        ],
                                      ),
                                      DropdownButton(
                                        isDense: true,
                                        value: actualDropdown,
                                        onChanged: (value) => setState(() {
                                          actualDropdown = value!;
                                          actualChart =
                                              chartDropdownItems.indexOf(
                                                  value); // Refresh the chart
                                        }),
                                        items: chartDropdownItems.map(
                                          (String title) {
                                            return DropdownMenuItem(
                                              value: title,
                                              child: Text(title,
                                                  style: const TextStyle(
                                                      color: Colors.blue,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      fontSize: 14.0)),
                                            );
                                          },
                                        ).toList(),
                                      )
                                    ],
                                  ),
                                  const Padding(
                                      padding: EdgeInsets.only(bottom: 4.0)),
                                  Sparkline(
                                    data: charts[actualChart],
                                    pointsMode: PointsMode.all,
                                    averageLine: true,
                                    useCubicSmoothing: true,
                                    fillMode: FillMode.below,
                                    lineWidth: 3.0,
                                    lineColor: Colors.greenAccent,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  },
                );
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Inventory Level For Next 7 Days Predicted!'),
            elevation: 2.0,
            backgroundColor: Colors.green,
          ),
        ),
        icon: const Icon(Icons.recycling_outlined),
        label: const Text('Predict Inventory'),
        backgroundColor: Colors.white60,
      ),
    );
  }
}

class InventoryItem {
  final String name;
  final double currentStock;
  final String unit;
  final List<double> salesHistory;

  InventoryItem(this.name, this.currentStock, this.salesHistory, this.unit);

  double predictInventory(double smoothingFactor, int forecastDays) {
    double predictedStock = currentStock;
    for (int i = 0; i < forecastDays; i++) {
      double forecast = salesHistory.last * smoothingFactor +
          (1 - smoothingFactor) * predictedStock;
      predictedStock = forecast;
    }
    return predictedStock;
  }
}

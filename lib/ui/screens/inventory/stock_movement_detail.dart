import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import '../../../controllers/product_controller.dart';
import '../../drawer/drawer_widget_menu.dart';

class StockMovementDetail extends StatefulWidget {
  const StockMovementDetail({super.key, required this.productId, required this.title, required this.inventory});

  final int productId;
  final String title;
  final Inventory inventory;

  @override
  State<StockMovementDetail> createState() => _StockMovementDetailState();
}

class _StockMovementDetailState extends State<StockMovementDetail> {
  String get title => widget.title;

  int get productId => widget.productId;

  Inventory get inventory => widget.inventory;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        leading: const DrawerWidgetMenu(icon: FaIcon(FontAwesomeIcons.arrowLeft)),
        title: Text(
          title,
          style: const TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 30.0),
        ),
      ),
      body: SafeArea(
        child: Center(
          child: FutureBuilder(
            future: Provider.of<ProductController>(context, listen: false).getStockValue(productId),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              return snapshot.connectionState == ConnectionState.waiting
                  ? const Center(child: CircularProgressIndicator())
                  : Consumer<ProductController>(
                      builder: (context, productController, child) {
                        String functionDate(date) {
                          print(date);
                          String oldDateString = date;
                          DateFormat oldFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
                          DateTime dateTime = oldFormat.parse(oldDateString);
                          DateFormat newFormat = DateFormat("dd/MM/yyyy");
                          String newDateString = newFormat.format(dateTime);
                          return newDateString;
                        }

                        var list = productController.stocksOnProduct
                            .map((e) => Inventory(
                                  stockMovement: e.stockMovement,
                                  date: '2024-05-03',
                                  quantity: e.quantity,
                                  units: inventory.units,
                                  closingStock: e.sellingPrice,
                                  productId: e.productId,
                                  price: e.buyingPrice,
                                  rate: double.parse(e.rate),
                                  entryType: e.entryType,
                                ))
                            .toList();
                        return LayoutBuilder(
                          builder: (BuildContext context, BoxConstraints viewportConstraints) {
                            return SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: ConstrainedBox(
                                constraints: BoxConstraints(
                                  minHeight: viewportConstraints.maxHeight,
                                ),
                                child: Column(
                                  children: [
                                    inventory,
                                    ...list,
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      },
                    );
            },
          ),
        ),
      ),
    );
  }
}

class Inventory extends StatelessWidget {
  final String stockMovement;
  final String date;
  final double quantity;
  final String units;
  final double price;
  final double rate;
  final int productId;
  final double closingStock;
  final String entryType;

  const Inventory(
      {super.key,
      required this.stockMovement,
      required this.date,
      required this.quantity,
      required this.units,
      required this.price,
      required this.productId,
      required this.rate,
      required this.closingStock,
      required this.entryType});

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Card.filled(
      color: Colors.white,
      child: SizedBox(
        width: width,
        height: 100,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    stockMovement,
                    style: TextStyle(
                        fontSize: 20.0, fontWeight: FontWeight.bold, color: stockMovement == "stockIn" ? Colors.green : Colors.red),
                  ),
                  Column(
                    children: [
                      const Text(
                        'Stock',
                        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w200, color: Colors.grey),
                      ),
                      Text(
                        '$quantity $units',
                        style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.blue),
                      ),
                    ],
                  ),
                ],
              ),
              const Center(
                child: Divider(
                  height: 10,
                  thickness: 0.8,
                  indent: 1,
                  endIndent: 1,
                  color: Colors.grey,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      Text(
                        entryType,
                        style: const TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                      ),
                      Text(
                        date,
                        style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                      ),
                    ],
                  ),
                  const VerticalDivider(
                    width: 20,
                    thickness: 1,
                    indent: 20,
                    endIndent: 0,
                    color: Colors.grey,
                  ),
                  Column(
                    children: [
                      Text(
                        'Price : ${price.toStringAsFixed(2)}',
                        style: const TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                      ),
                      Text(
                        'Rate : 1.0 USD = ${rate.toStringAsFixed(2)}',
                        style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                      ),
                    ],
                  ),
                  const VerticalDivider(
                    width: 20,
                    thickness: 1,
                    indent: 20,
                    endIndent: 0,
                    color: Colors.grey,
                  ),
                  Column(
                    children: [
                      const Text(
                        'Price',
                        style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                      ),
                      Text(
                        '${(price / rate).toStringAsFixed(3)} USD',
                        style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

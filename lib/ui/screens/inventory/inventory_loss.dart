import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import '../../../controllers/inventory_loss.controller.dart';
import '../../../controllers/product_controller.dart';
import '../../../data/local/product_loss.model.dart';
import '../../drawer/drawer_widget_menu.dart';
import '../../widgets/date_input.dart';
import '../../widgets/dropdown.dart';
import '../../widgets/form.dart';
import '../../widgets/form_input.dart';
import 'add_inventory.dart';

class InventoryLoss extends StatefulWidget {
  const InventoryLoss({super.key});

  @override
  State<InventoryLoss> createState() => _InventoryLossState();
}

class _InventoryLossState extends State<InventoryLoss> {
  final _formKey = GlobalKey<FormState>();

  //controllers
  final dateCaptureController = TextEditingController();
  final productControllerText = TextEditingController();
  final reasonForLossController = TextEditingController();
  final descriptionController = TextEditingController();
  final quantityController = TextEditingController();

  //focus node
  late FocusNode descriptionFocusNode;
  late FocusNode quantityFocusNode;

  final int year = DateTime.now().year;
  final int month = DateTime.now().month;
  final int day = DateTime.now().day;

  @override
  void initState() {
    super.initState();
    dateCaptureController.text = '$day/$month/$year';

    //initialize focus node
    descriptionFocusNode = FocusNode();
    quantityFocusNode = FocusNode();
  }

  @override
  void dispose() {
    //dispose controllers
    dateCaptureController.dispose();
    productControllerText.dispose();
    reasonForLossController.dispose();
    descriptionController.dispose();
    quantityController.dispose();

    //dispose focus node
    descriptionFocusNode.dispose();
    quantityFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        leading:
            const DrawerWidgetMenu(icon: FaIcon(FontAwesomeIcons.arrowLeft)),
        title: const Text(
          'Inventory Loss',
          style: TextStyle(
              color: Colors.black, fontWeight: FontWeight.w700, fontSize: 30.0),
        ),
      ),
      body: SafeArea(
        child: Card(
          color: Colors.white,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(1.0)),
          child: FutureBuilder(
            future: Provider.of<ProductController>(context, listen: false)
                .getProducts(),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              return snapshot.connectionState == ConnectionState.waiting
                  ? const Center(child: CircularProgressIndicator())
                  : Consumer<ProductController>(
                      builder: (context, productController, child) {
                        return CustomForm(
                          formInputs: [
                            DateInput(
                              controller: dateCaptureController,
                              contentPadding: 1.0,
                              iconColorS: Colors.blue,
                            ),
                            SizedBox(
                                width: MediaQuery.of(context).size.width * 0.90,
                                child: Padding(
                                  padding: const EdgeInsets.all(1.0),
                                  child: Center(
                                      child: CustomDropdownMenu(
                                          products: productController.products,
                                          controller: productControllerText)),
                                )),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.90,
                              child: Padding(
                                padding: const EdgeInsets.all(1.0),
                                child: Center(
                                  child: CustomDropDown<DamageEnum>(
                                    items: DamageEnum.values
                                        .map<DropdownMenuEntry<DamageEnum>>(
                                          (DamageEnum icon) =>
                                              DropdownMenuEntry<DamageEnum>(
                                            style: ButtonStyle(
                                              textStyle: MaterialStateProperty
                                                  .all<TextStyle>(
                                                const TextStyle(
                                                    fontStyle: FontStyle.normal,
                                                    color: Colors.black,
                                                    fontWeight:
                                                        FontWeight.w300),
                                              ),
                                            ),
                                            value: icon,
                                            label: icon.label,
                                            leadingIcon: Text(
                                              CurrencyUtil()
                                                  .currencyEmoji('damage'),
                                              style: const TextStyle(
                                                  fontStyle: FontStyle.normal,
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w300),
                                            ),
                                          ),
                                        )
                                        .toList(),
                                    controller: reasonForLossController,
                                    label: const Text("Reason For Loss"),
                                    leadingIcon: const Icon(
                                        FontAwesomeIcons.deleteLeft,
                                        color: Colors.black),
                                  ),
                                ),
                              ),
                            ),
                            InputForm(
                              label: 'Quantity',
                              prefix: const Icon(FontAwesomeIcons.fileInvoice),
                              labelStyle: const TextStyle(
                                  fontStyle: FontStyle.normal,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300),
                              focusNode: quantityFocusNode,
                              inputType: TextInputType.number,
                              controller: quantityController,
                              contentPadding: 1.0,
                              border: const OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.black, width: 1.0),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(1.0)),
                                gapPadding: 2.0,
                              ),
                            ),
                            InputForm(
                              hintText: 'Description of Stock Movement',
                              hintStyle: const TextStyle(
                                  fontStyle: FontStyle.normal,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300),
                              hintTextDirection: TextDirection.ltr,
                              maxLines: 4,
                              labelStyle: const TextStyle(
                                  fontStyle: FontStyle.normal,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300),
                              inputType: TextInputType.multiline,
                              focusNode: descriptionFocusNode,
                              controller: descriptionController,
                              contentPadding: 2.0,
                              border: const OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.black, width: 1.0),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(1.0)),
                                gapPadding: 2.0,
                              ),
                            ),
                            FloatingActionButton.extended(
                              backgroundColor: Colors.blue,
                              onPressed: () {
                                //validate form
                                if (_formKey.currentState!.validate()) {
                                  var date = dateCaptureController.text;
                                  var reasonForLoss =
                                      reasonForLossController.text;
                                  var description = descriptionController.text;
                                  var product = productControllerText.text;
                                  var quantity =
                                      double.parse(quantityController.text);

                                  //split product to get product id
                                  var productId = product.split('|').first;

                                  // print(
                                  //     'Product ID: $productId Date: $date Reason: $reasonForLoss Description: $description Product: $product');

                                  ProductLossModel productLoss =
                                      ProductLossModel(
                                    productId: int.parse(productId),
                                    quantity: quantity,
                                    averageCostOfProduct: 0.0,
                                    totalCost: 0.0,
                                    reasonForLoss: reasonForLoss,
                                    description: description,
                                    dateCapture: date,
                                    product: product,
                                  );

                                  //get inventory loss controller
                                  InventoryLossController();
                                  //add inventory loss
                                  Provider.of<InventoryLossController>(context,
                                          listen: false)
                                      .addInventoryLoss(productLoss);
                                  //clear form
                                  // dateCaptureController.clear();
                                  productControllerText.clear();
                                  descriptionController.clear();
                                  reasonForLossController.clear();
                                  quantityController.clear();

                                  //show success message
                                  ScaffoldMessenger.of(context).showSnackBar(
                                    const SnackBar(
                                      content: Text(
                                          'Stock Movement Added Successfully'),
                                      elevation: 2.0,
                                      backgroundColor: Colors.green,
                                    ),
                                  );
                                }
                              },
                              label: const Text('Record Loss'),
                              icon: const Icon(Icons.add),
                            ),
                          ],
                          formKey: _formKey,
                        );
                      },
                    );
            },
          ),
        ),
      ),
    );
  }
}

enum DamageEnum {
  //currency flags
  damage('Damage', 'damage'),
  wastage('Wastage', 'wastage'),
  expiry('Expiry', 'expiry'),
  dryingWeightLoss('Drying Weight Loss', 'dryingWeightLoss'),
  fire('Fire', 'fire'),
  theft('Theft', 'theft'),
  shortSupplied('Short Supplied', 'shortSupplied'),
  internalCountingError('Internal Counting Error', 'internalCountingError'),
  other('Other', 'other');

  const DamageEnum(this.label, this.dbValue);

  final String label;
  final String dbValue;
}

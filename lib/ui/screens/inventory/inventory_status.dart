import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:retail/ui/screens/inventory/stock_movement_detail.dart';
import '../../../controllers/product_controller.dart';
import '../../../data/local/product.model.dart';
import '../../drawer/drawer_widget_menu.dart';

class InventoryStatus extends StatefulWidget {
  const InventoryStatus({super.key});

  @override
  State<InventoryStatus> createState() => _InventoryStatusState();
}

class _InventoryStatusState extends State<InventoryStatus> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        leading: const DrawerWidgetMenu(icon: FaIcon(FontAwesomeIcons.arrowLeft)),
        title: const Text(
          'Inventory Status',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 30.0),
        ),
      ),
      body: FutureBuilder(
        future: Provider.of<ProductController>(context, listen: false).initialize(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return snapshot.connectionState == ConnectionState.waiting
              ? const Center(child: CircularProgressIndicator())
              : Consumer<ProductController>(
                  builder: (context, productController, child) {
                    return ListView.builder(
                      itemCount: productController.products.length,
                      itemBuilder: (context, index) {
                        var stocks = productController.stockMovements
                            .where((element) => element.productId == productController.products[index].primaryKey)
                            .toList();
                        double inStock =
                            stocks.where((element) => element.stockMovement == 'stockIn').map((e) => e.quantity).fold(0, (a, b) => a + b);
                        double outStock =
                            stocks.where((element) => element.stockMovement == 'stockOut').map((e) => e.quantity).fold(0, (a, b) => a + b);

                        double inStockTotalValue = stocks
                            .where((element) => element.stockMovement == 'stockIn')
                            .map((e) => (e.quantity * (e.buyingPrice / double.parse(e.rate))))
                            .fold(0, (a, b) => a + b);

                        double outStockTotalValue = stocks
                            .where((element) => element.stockMovement == 'stockOut')
                            .map((e) => (e.quantity * (e.buyingPrice / double.parse(e.rate))))
                            .fold(0, (a, b) => a + b);

                        //stock using average method
                        return InventoryCard(
                          itemName: productController.products[index].productName,
                          openingStock: double.parse(productController.products[index].quantity),
                          price: (double.parse(productController.products[index].quantity) *
                                  (double.parse(productController.products[index].purchasePrice) /
                                      productController.products[index].rate)) +
                              inStockTotalValue -
                              outStockTotalValue,
                          inStock: inStock,
                          outStock: outStock,
                          closingStock: (double.parse(productController.products[index].quantity)) + inStock - outStock,
                          productId: productController.products[index].primaryKey!,
                          product: productController.products[index],
                        );
                      },
                    );
                  },
                );
        },
      ),
    );
  }
}

class InventoryCard extends StatelessWidget {
  final String itemName;
  final double openingStock;
  final double price;
  final double inStock;
  final double outStock;
  final double closingStock;
  final int productId;
  final ProductModel product;

  const InventoryCard(
      {super.key,
      required this.itemName,
      required this.openingStock,
      required this.price,
      required this.inStock,
      required this.outStock,
      required this.closingStock,
      required this.productId,
      required this.product});

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => StockMovementDetail(
            productId: productId,
            title: itemName,
            inventory: Inventory(
              stockMovement: 'Opening Stock',
              date: '2024-05-03',
              quantity: double.parse(product.quantity),
              units: product.measurementUnit,
              price: double.parse(product.purchasePrice),
              productId: product.primaryKey!,
              rate: product.rate,
              closingStock: 0.0,
              entryType: 'MANUAL',
            ),
          ),
        ),
      ),
      child: Card.filled(
        color: Colors.white,
        child: SizedBox(
          width: width,
          height: 100,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      itemName,
                      style: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                    ),
                    Column(
                      children: [
                        const Text(
                          'Stock Value',
                          style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w200, color: Colors.grey),
                        ),
                        Text(
                          'USD ${price.toStringAsFixed(2)}',
                          style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.blue),
                        ),
                      ],
                    ),
                  ],
                ),
                const Center(
                  child: Divider(
                    height: 10,
                    thickness: 0.8,
                    indent: 1,
                    endIndent: 1,
                    color: Colors.grey,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        const Text(
                          'Opening Stock',
                          style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                        ),
                        Text(
                          '$openingStock',
                          style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                        ),
                      ],
                    ),
                    const VerticalDivider(
                      width: 20,
                      thickness: 1,
                      indent: 20,
                      endIndent: 0,
                      color: Colors.grey,
                    ),
                    Column(
                      children: [
                        const Text(
                          'In',
                          style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                        ),
                        Text(
                          '$inStock',
                          style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                        ),
                      ],
                    ),
                    const VerticalDivider(
                      width: 20,
                      thickness: 1,
                      indent: 20,
                      endIndent: 0,
                      color: Colors.grey,
                    ),
                    Column(
                      children: [
                        const Text(
                          'Out',
                          style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                        ),
                        Text(
                          '$outStock',
                          style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                        ),
                      ],
                    ),
                    const VerticalDivider(
                      width: 20,
                      thickness: 1,
                      indent: 20,
                      endIndent: 0,
                      color: Colors.grey,
                    ),
                    Column(
                      children: [
                        const Text(
                          'Closing Stock',
                          style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                        ),
                        Text(
                          '$closingStock',
                          style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

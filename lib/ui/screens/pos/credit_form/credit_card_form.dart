import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:text_form_field_wrapper/text_form_field_wrapper.dart';
import '../../../../controllers/cart.dart';
import '../../../../controllers/receipt.sales.dart';
import '../../../../data/local/receipt.model.dart';
import '../../../widgets/dropdown.dart';
import '../../inventory/add_inventory.dart';
import '../models/card_form_results.dart';
import '../point_of_sale.index.dart';
import '../ui_components/pay_button.dart';
import 'validation.dart';

enum IconLabel {
  //currency flags
  usd('United States Dollar | USD', 'USA'),
  zwl('Zimbabwe Dollar | ZIG', 'ZWL'),
  zar('South African Rand | ZAR', 'ZAR'),
  bwp('Botswana Pula | BWP', 'BWP'),
  mzn('Mozambique Metical | MZN', 'MZN'),
  zmw('Zambian Kwacha | ZMW', 'ZMW'),
  gbp('British Pound | GBP', 'GBP'),
  eur('Euro | EUR', 'EUR'),
  inr('Indian Rupee | INR', 'INR'),
  aud('Australian Dollar | AUD', 'AUD'),
  nzd('New Zealand Dollar | NZD', 'NZD'),
  cad('Canadian Dollar | CAD', 'CAD');

  const IconLabel(this.label, this.icon);

  final String label;
  final String icon;
}

enum PaymentMethod {
  card,
  cash,
  mobile,
  bank,
  ecoCash,
  oneMoney,
  teleCash,
  visa,
  masterCard,
  innBucks,
}

class CreditCardForm extends StatefulWidget {
  /// The CreditCardForm is a stateful widget containing a form and fields
  /// necessary to complete a typical credit card transaction
  const CreditCardForm({
    super.key,
    this.payBtnKey,
    this.formKey,
    this.displayPhone = true,
    this.submitLabel = 'Submit',
    required this.amount,
  });

  final double amount;

  /// If you would like to control the pay button state to display text or icons
  /// based on the current stage of the payment process, you will need to
  /// provide a [CardPayButtonState] key to update it.
  final GlobalKey<CardPayButtonState>? payBtnKey;

  /// You will need to provide a general [FormState] key to control, validate
  /// and save the form data based on your needs.
  final GlobalKey<FormState>? formKey;

  final bool displayPhone;

  final String submitLabel;

  @override
  State<CreditCardForm> createState() => _CreditCardFormState();
}

class _CreditCardFormState extends State<CreditCardForm> {
  late final GlobalKey<FormState> _formKey; // = GlobalKey<FormState>();
  late final GlobalKey<CardPayButtonState>? payBtnKey;
  int chosenCountryIndex = 0;

  TextEditingController cCurrency = TextEditingController();
  TextEditingController cRate = TextEditingController();
  TextEditingController cName = TextEditingController();
  TextEditingController cZip = TextEditingController();
  TextEditingController cPhone = TextEditingController();
  TextEditingController cAmount = TextEditingController();
  TextEditingController paymentMethod = TextEditingController();

  late FocusNode rateFocusNode;
  late FocusNode nameFocusNode;
  late FocusNode zipFocusNode;
  late FocusNode phoneFocusNode;
  late FocusNode amountFocusNode;

  double get amount => widget.amount;

  @override
  void initState() {
    super.initState();
    if (widget.formKey != null) {
      _formKey = widget.formKey!;
    } else {
      _formKey = GlobalKey<FormState>();
    }

    payBtnKey = widget.payBtnKey;

    cRate.addListener(() {
      calculateTotalAmount();
    });

    rateFocusNode = FocusNode();
    nameFocusNode = FocusNode();
    zipFocusNode = FocusNode();
    phoneFocusNode = FocusNode();
    amountFocusNode = FocusNode();
  }

  @override
  void dispose() {
    super.dispose();
    cCurrency.dispose();
    cRate.dispose();
    cName.dispose();
    cZip.dispose();
    cPhone.dispose();
    cAmount.dispose();
    paymentMethod.dispose();

    rateFocusNode.dispose();
    nameFocusNode.dispose();
    zipFocusNode.dispose();
    phoneFocusNode.dispose();
    amountFocusNode.dispose();
  }

  void calculateTotalAmount() {
    // Parse the rate and amount to double
    double rate = double.tryParse(cRate.text) ?? 0.0;
    double amo = amount;

    // Calculate the total amount
    double totalAmount = rate * amo;

    // Update the total amount controller
    cAmount.text = totalAmount.toStringAsFixed(2);
  }

  void clearForm() {
    cCurrency.clear();
    cRate.clear();
    cName.clear();
    cZip.clear();
    cPhone.clear();
    cAmount.clear();
    paymentMethod.clear();
  }

  @override
  Widget build(BuildContext context) {
    // List<CartModel> uniqueProducts = Provider.of<CartController>(context, listen: false).uniqueProducts;
    return FutureBuilder(
        future: null,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return snapshot.connectionState == ConnectionState.waiting
              ? const Center(child: LinearProgressIndicator())
              : Consumer<CartController>(builder: (context, cartController, child) {
                print(cartController.uniqueProducts);
                  return Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        const Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(4, 16, 0, 4),
                              child: Text('Select Paying Method'),
                            ),
                          ],
                        ),
                        CustomDropDown<PaymentMethod>(
                          items: PaymentMethod.values
                              .map<DropdownMenuEntry<PaymentMethod>>((PaymentMethod icon) => DropdownMenuEntry<PaymentMethod>(
                                    style: ButtonStyle(
                                      textStyle: MaterialStateProperty.all<TextStyle>(
                                        const TextStyle(fontStyle: FontStyle.normal, color: Colors.black, fontWeight: FontWeight.w300),
                                      ),
                                    ),
                                    value: icon,
                                    label: icon.toString().split('.').last,
                                    // leadingIcon: Text(
                                    //   CurrencyUtil().currencyEmoji(icon.icon),
                                    //   style: const TextStyle(fontStyle: FontStyle.normal, color: Colors.black, fontWeight: FontWeight.w300),
                                    // ),
                                  ))
                              .toList(),
                          controller: paymentMethod,
                          // label: const Text("Select Payment Method"),
                          leadingIcon: const Icon(FontAwesomeIcons.paypal),
                        ),
                        const Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(4, 16, 0, 4),
                              child: Text('Select Paying Currency'),
                            ),
                          ],
                        ),
                        CustomDropDown<IconLabel>(
                          items: IconLabel.values
                              .map<DropdownMenuEntry<IconLabel>>(
                                (IconLabel icon) => DropdownMenuEntry<IconLabel>(
                                  style: ButtonStyle(
                                    textStyle: MaterialStateProperty.all<TextStyle>(
                                      const TextStyle(fontStyle: FontStyle.normal, color: Colors.black, fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                  value: icon,
                                  label: icon.label,
                                  leadingIcon: Text(
                                    CurrencyUtil().currencyEmoji(icon.icon),
                                    style: const TextStyle(fontStyle: FontStyle.normal, color: Colors.black, fontWeight: FontWeight.w300),
                                  ),
                                ),
                              )
                              .toList(),
                          controller: cCurrency,
                          // label: const Text("Select Currency"),
                          leadingIcon: const Icon(FontAwesomeIcons.moneyCheckDollar),
                        ),
                        const Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(4, 16, 0, 4),
                              child: Text('Rate Used'),
                            ),
                          ],
                        ),
                        TextFormFieldWrapper(
                          formField: TextFormField(
                            focusNode: rateFocusNode,
                            controller: cRate,
                            keyboardType: TextInputType.number,
                            validator: (input) {
                              if (input!.isNotEmpty) {
                                return null;
                              } else {
                                return 'Enter Rate';
                              }
                            },
                            decoration: const InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 16),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(4, 16, 0, 4),
                              child: Text('Amount To Pay'),
                            ),
                          ],
                        ),
                        TextFormFieldWrapper(
                          formField: TextFormField(
                            readOnly: true,
                            controller: cAmount,
                            focusNode: amountFocusNode,
                            keyboardType: TextInputType.number,
                            validator: (input) {
                              if (input!.isNotEmpty) {
                                return null;
                              } else {
                                return 'Enter Amount';
                              }
                            },
                            decoration: const InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 16),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        const Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(4, 16, 0, 4),
                              child: Text('Name Of Customer'),
                            ),
                          ],
                        ),
                        TextFormFieldWrapper(
                          formField: TextFormField(
                            controller: cName,
                            focusNode: nameFocusNode,
                            keyboardType: TextInputType.name,
                            validator: (input) {
                              if (input!.isNotEmpty) {
                                return null;
                              } else {
                                return 'Enter a valid name';
                              }
                            },
                            decoration: const InputDecoration(
                              contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 16),
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        if (widget.displayPhone)
                          const Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.fromLTRB(4, 16, 0, 4),
                                child: Text('Phone Number'),
                              ),
                            ],
                          ),
                        if (widget.displayPhone)
                          TextFormFieldWrapper(
                            position: TextFormFieldPosition.alone,
                            formField: TextFormField(
                              controller: cPhone,
                              focusNode: phoneFocusNode,
                              keyboardType: TextInputType.number,
                              validator: (input) {
                                if (input!.isNotEmpty) return null;
                                return 'Enter a valid phone number';
                              },
                              inputFormatters: [
                                MaskedTextInputFormatter(
                                  mask: 'xxx-xxx-xxxx',
                                  separator: '-',
                                )
                              ],
                              decoration: const InputDecoration(
                                // hintText: 'Phone',
                                contentPadding: EdgeInsets.symmetric(vertical: 5, horizontal: 16),
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        const SizedBox(
                          height: 30,
                        ),
                        CardPayButton(
                          key: payBtnKey,
                          initStatus: CardPayButtonStatus.ready,
                          text: widget.submitLabel,
                          onPressed: () {
                            bool result = _formKey.currentState!.validate();
                            if (result) {
                              _formKey.currentState!.save();

                              var receipt = Receipt(
                                  currencyName: cCurrency.text,
                                  currencyCode: cCurrency.text,
                                  rate: double.parse(cRate.text),
                                  customerName: cName.text,
                                  customerPhoneNumber: cPhone.text,
                                  totalAmount: cAmount.text,
                                  status: 'success');

                              print(cartController.uniqueProducts.length);
                              //create a receipt
                              ReceiptController().createReceipt(receipt, [...cartController.uniqueProducts]);
                              //clear cart
                              cartController.clearCart();
                              // clear form
                              clearForm;
                              // show success message
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(
                                  content: Text('Data Saved Successfully'),
                                  backgroundColor: Colors.green,
                                ),
                              );

                              Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(builder: (context) => const PointOfSaleIndex()),
                                (Route<dynamic> route) => false,
                              );
                            }
                          },
                        ),
                      ],
                    ),
                  );
                });
        });
  }
}

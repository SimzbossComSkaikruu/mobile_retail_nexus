import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:retail/ui/screens/pos/ui_components/footer.dart';
import 'package:retail/ui/screens/pos/ui_components/pay_button.dart';

import '../../../controllers/cart.dart';
import 'checkout_page/checkout_page.dart';
import 'models/price_item.dart';

class ProcessPayment extends StatelessWidget {
  const ProcessPayment({super.key});

  @override
  Widget build(BuildContext context) {
    /// RECOMMENDED: A global Key to access the credit card pay button options
    ///
    /// If you want to interact with the payment button icon, you will need to
    /// create a global key to pass to the checkout page. Without this key
    /// the the button will always display 'Pay'. You may view several ways to
    /// interact with the button elsewhere within this example.
    final GlobalKey<CardPayButtonState> payBtnKey = GlobalKey<CardPayButtonState>();


    /// REQUIRED: A name representing the receiver of the funds from user
    ///
    /// Demo vendor name provided here. User's need to know who is receiving
    /// their money
    const String payToName = 'Process Payment';

    /// REQUIRED: (if you are using the native pay options)
    ///
    /// Determine whether this platform is iOS. This affects which native pay
    /// option appears. This is the most basic form of logic needed. You adjust
    /// this logic based on your app's needs and the platforms you are
    /// developing for.
    const isApple = false;

    /// RECOMMENDED: widget to display at footer of page
    ///
    /// Apple and Google stores typically require a link to privacy and terms when
    /// your app is collecting and/or transmitting sensitive data. This link is
    /// expected on the same page as the form that the user is filling out. You
    /// can make this any type of widget you want, but we have created a prebuilt
    /// [CheckoutPageFooter] widget that just needs the corresponding links
    const footer = CheckoutPageFooter(
      // These are example url links only. Use your own links in live code
      privacyLink: 'https://[Credit Processor].com/privacy',
      termsLink: 'https://[Credit Processor].com/payment-terms/legal',
      note: 'Powered By [Credit Processor]',
      noteLink: 'https://[Credit Processor].com/',
    );

    /// OPTIONAL: A function for the back button
    ///
    /// This to be used as needed. If you have another back button built into your
    /// app, you can leave this function null. If you need a back button function,
    /// simply add the needed logic here. The minimum required in a simple
    /// Navigator.of(context).pop() request
    Function? onBack = Navigator.of(context).canPop() ? () => Navigator.of(context).pop() : null;

    return Scaffold(
      appBar: null,
      body: FutureBuilder(
        future: null,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return snapshot.connectionState == ConnectionState.waiting
              ? const Center(child: CircularProgressIndicator())
              : Consumer<CartController>(
                  builder: (context, cartController, child) {
                    List<PriceItem> priceItems = cartController.uniqueProducts
                        .map((e) =>
                            PriceItem(name: e.product.productName, quantity: e.quantityProduct, itemCostCents: double.parse(e.product.price)))
                        .toList();

                    return CheckoutPage(
                      data: CheckoutData(
                        priceItems: priceItems,
                        payToName: payToName,
                        displayNativePay: !kIsWeb,
                        isApple: isApple,
                        onBack: onBack,
                        payBtnKey: payBtnKey,
                        displayTestData: true,
                      ),
                      // footer: footer,
                    );
                  },
                );
        },
      ),
    );
  }
}

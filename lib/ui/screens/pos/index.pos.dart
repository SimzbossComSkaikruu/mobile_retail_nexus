import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:retail/ui/screens/pos/point_of_sale.index.dart';
import 'package:retail/ui/screens/pos/prdouct.prices.dart';
import 'package:retail/ui/screens/pos/sales.pre.trends.dart';
import '../../drawer/drawer_widget_menu.dart';
import '../../widgets/custom_widgets.dart';
import 'charts/daily.sale.dart';
import 'charts/line.pos.dart';
import 'charts/sales.predictions.dart';

class PosIndex extends StatefulWidget {
  const PosIndex({super.key, required this.openDrawer});

  final VoidCallback openDrawer;

  @override
  State<PosIndex> createState() => _PosIndexState();
}

class _PosIndexState extends State<PosIndex> {
  VoidCallback get openDrawer => widget.openDrawer;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        leading: DrawerWidgetMenu(onClicked: openDrawer),
        title: const Text(
          'Sale Dashboard',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 30.0),
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                StaggeredGrid.count(
                  crossAxisCount: 4,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10,
                  children: <Widget>[
                    StaggeredGridTile.count(
                      crossAxisCellCount: 2,
                      mainAxisCellCount: 2,
                      child: InkWell(
                        onTap: () => _pushScreen(
                          context: context,
                          screen: const PointOfSaleIndex(),
                        ),
                        child: const WidgetsCustom(
                          child: Padding(
                            padding: EdgeInsets.all(24.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Material(
                                  color: Colors.blue,
                                  shape: CircleBorder(),
                                  child: Padding(
                                    padding: EdgeInsets.all(16.0),
                                    child: Icon(Icons.monetization_on_outlined, color: Colors.white, size: 25.0),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 16.0),
                                ),
                                Text('New Sale', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 20.0)),
                                Text(
                                  'Create New Sale',
                                  style: TextStyle(color: Colors.black45, fontSize: 10.0, fontWeight: FontWeight.w700),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    StaggeredGridTile.count(
                      crossAxisCellCount: 2,
                      mainAxisCellCount: 2,
                      child: InkWell(
                        onTap: () => _pushScreen(
                          context: context,
                          screen: const ProductPrice(),
                        ),
                        child: const WidgetsCustom(
                          child: Padding(
                            padding: EdgeInsets.all(24.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Material(
                                  color: Colors.blue,
                                  shape: CircleBorder(),
                                  child: Padding(
                                    padding: EdgeInsets.all(16.0),
                                    child: Icon(Icons.clean_hands_outlined, color: Colors.white, size: 25.0),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(bottom: 16.0),
                                ),
                                Text('Prices', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 20.0)),
                                Text(
                                  'Set Prices',
                                  style: TextStyle(color: Colors.black45, fontSize: 10.0, fontWeight: FontWeight.w700),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const Padding(
                  padding: EdgeInsets.only(top: 8.0),
                  child: WidgetsCustom(child: LineChartMonthlySale()),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: InkWell(
                      onTap: () => _pushScreen(
                            context: context,
                            screen: const InventoryTrends(),
                          ),
                      child: const SalesPrediction()),
                  // child: WidgetsCustom(child: LineChartDaily()),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

void _pushScreen({required BuildContext context, required Widget screen}) {
  ThemeData themeData = Theme.of(context);
  Navigator.push(
    context,
    MaterialPageRoute(
      builder: (_) => Theme(data: themeData, child: screen),
    ),
  );
}

import 'package:chart_sparkline/chart_sparkline.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../../controllers/receipt.sales.dart';
import '../../../widgets/custom_widgets.dart';

class SalesPrediction extends StatefulWidget {
  const SalesPrediction({super.key});

  @override
  State<SalesPrediction> createState() => _SalesPredictionState();
}

class _SalesPredictionState extends State<SalesPrediction> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Provider.of<ReceiptController>(context, listen: false).initReceipts(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return snapshot.connectionState == ConnectionState.waiting
            ? const CircularProgressIndicator()
            : Consumer<ReceiptController>(
                builder: (context, productController, child) {
                  double sum = 0;

                  List<double> data = [sum];

                  for (var receipt in productController.receipts) {
                    sum = sum + (double.parse(receipt.totalAmount.toString()) / double.parse(receipt.rate.toString()));
                    data.add(sum);
                  }

                  final List<List<double>> charts = [
                    [
                      ...data,
                    ], // Last 7 days
                  ];

                  //chart dropdown items
                  final List<String> chartDropdownItems = [
                    'Last 7 days',
                    // 'Last month',// 'Last 3 months', // 'Last 6 months',
                    // 'Last year',
                  ];
                  String actualDropdown = chartDropdownItems[0];
                  int actualChart = 0;

                  return WidgetsCustom(
                    child: Padding(
                      padding: const EdgeInsets.all(24.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  const Text('Total Sales', style: TextStyle(color: Colors.green)),
                                  Text(
                                    '\$${sum.toStringAsFixed(2)} usd',
                                    style: const TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 34.0),
                                  ),
                                ],
                              ),
                              DropdownButton(
                                isDense: true,
                                value: actualDropdown,
                                onChanged: (value) => setState(() {
                                  actualDropdown = value!;
                                  actualChart = chartDropdownItems.indexOf(value); // Refresh the chart
                                }),
                                items: chartDropdownItems.map((String title) {
                                  return DropdownMenuItem(
                                    value: title,
                                    child: Text(title,
                                        style: const TextStyle(color: Colors.blue, fontWeight: FontWeight.w400, fontSize: 14.0)),
                                  );
                                }).toList(),
                              )
                            ],
                          ),
                          const Padding(padding: EdgeInsets.only(bottom: 4.0)),
                          Sparkline(
                            data: charts[actualChart],
                            lineWidth: 5.0,
                            lineColor: Colors.greenAccent,
                          )
                        ],
                      ),
                    ),
                  );
                },
              );
      },
    );
  }
}

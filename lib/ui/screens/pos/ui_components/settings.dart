import 'package:flutter/material.dart';

import '../../../drawer/drawer_widget_menu.dart';
import '../../inventory/add_inventory.dart';

class Settings extends StatelessWidget {
  const Settings({super.key, required this.openDrawer});

  final VoidCallback openDrawer;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          elevation: 2.0,
          backgroundColor: Colors.white,
          leading: DrawerWidgetMenu(onClicked: openDrawer),
          title: const Text(
            'Settings',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 30.0),
          ),
        ),
        body: Card(
          color: Colors.white,
          child: SizedBox(
            width: MediaQuery.of(context).size.width, // Full width
            height: MediaQuery.of(context).size.height, // Full height
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      const Text(
                        'Base Currency',
                        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.grey),
                      ),
                      Text(
                        'USD ${CurrencyUtil().currencyEmoji('usd')}',
                        style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700, color: Colors.blue),
                      ),
                    ],
                  ),
                  const Center(
                    child: Divider(
                      height: 10,
                      thickness: 0.8,
                      indent: 1,
                      endIndent: 1,
                      color: Colors.grey,
                    ),
                  ),
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Stock Valuation',
                        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.grey),
                      ),
                      Text(
                        'Weighted Average Cost',
                        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700, color: Colors.blue),
                      ),
                    ],
                  ),
                  const Center(
                    child: Divider(
                      height: 10,
                      thickness: 0.8,
                      indent: 1,
                      endIndent: 1,
                      color: Colors.grey,
                    ),
                  ),
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Model Running',
                        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.grey),
                      ),
                      Text(
                        'ARIMA Model',
                        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700, color: Colors.blue),
                      ),
                    ],
                  ),
                  const Center(
                    child: Divider(
                      height: 10,
                      thickness: 0.8,
                      indent: 1,
                      endIndent: 1,
                      color: Colors.grey,
                    ),
                  ),
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'App Version',
                        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.grey),
                      ),
                      Text(
                        '1.0.0',
                        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700, color: Colors.blue),
                      ),
                    ],
                  ),
                  const Center(
                    child: Divider(
                      height: 10,
                      thickness: 0.8,
                      indent: 1,
                      endIndent: 1,
                      color: Colors.grey,
                    ),
                  ),
                  const Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Developed By',
                        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.grey),
                      ),
                      Text(
                        'SimzbossComSkaikruu',
                        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700, color: Colors.blue),
                      ),
                    ],
                  ),
                  const Center(
                    child: Divider(
                      height: 10,
                      thickness: 0.8,
                      indent: 1,
                      endIndent: 1,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}

import 'package:chart_sparkline/chart_sparkline.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

import '../../../controllers/product_controller.dart';
import '../../../controllers/receipt.sales.dart';
import '../../../data/local/receipt.model.dart';
import '../../../data/local/receipt_line.model.dart';
import '../../../data/local/stock_movement.model.dart';
import '../../drawer/drawer_widget_menu.dart';
import '../../widgets/custom_widgets.dart';

class InventoryTrends extends StatefulWidget {
  const InventoryTrends({super.key});

  @override
  State<InventoryTrends> createState() => _InventoryTrendsState();
}

class _InventoryTrendsState extends State<InventoryTrends> {
  double smoothingFactor = 0.5; // Adjust as needed
  int forecastDays = 7;
  List<Receipt> receipts = [];

  @override
  // initState() {
  //   super.initState();
  //   receiptF();
  // }
  //
  // receiptF() async {
  //   var data = await Provider.of<ReceiptController>(context, listen: false).initReceipts();
  //   for (var item in data) {
  //     receipts.add(Receipt.fromJson(item));
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        leading: const DrawerWidgetMenu(icon: FaIcon(FontAwesomeIcons.arrowLeft)),
        title: const Text(
          'Sales Trends',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 30.0),
        ),
      ),
      body: FutureBuilder(
        future: Provider.of<ReceiptController>(context, listen: false).initReceipts(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return snapshot.connectionState == ConnectionState.waiting
              ? const Center(child: CircularProgressIndicator())
              : Consumer<ReceiptController>(
                  builder: (context, productController, child) {
                    List<InventoryItem> inventoryData = [];

                    List<double> computeCumulativeSum(double initialValue, List<ReceiptLineModel> inputList) {
                      final List<double> result = [initialValue];
                      double sum = initialValue;

                      for (final ReceiptLineModel value in inputList) {
                        sum += value.price;
                        result.add(sum);
                      }
                      return result;
                    }

                    for (var product in productController.products) {
                      var salesPerProduct =
                          productController.receiptLines.where((element) => element.productId == product.primaryKey).toList();

                      print(computeCumulativeSum(double.parse(product.price), [...salesPerProduct]));
                      inventoryData.add(
                        InventoryItem(
                          product.productName,
                          double.parse(product.price),
                          [
                            ...computeCumulativeSum(double.parse(product.price), [...salesPerProduct])
                          ],
                          "usd",
                        ),
                      );
                    }

                    return ListView.builder(
                      shrinkWrap: true,
                      itemCount: inventoryData.length,
                      itemBuilder: (context, index) {
                        InventoryItem item = inventoryData[index];
                        double predictedStock = item.predictInventory(smoothingFactor, forecastDays);

                        //charts graph data
                        final List<List<double>> charts = [
                          [for (int i = 0; i < item.salesHistory.length; i++) item.salesHistory[i], predictedStock], // Last 7 days
                        ];

                        // print(charts);
                        //chart dropdown items
                        final List<String> chartDropdownItems = [
                          'Last 7 days',
                          // 'Last month',// 'Last 3 months', // 'Last 6 months',
                          // 'Last year',
                        ];
                        String actualDropdown = chartDropdownItems[0];
                        int actualChart = 0;

                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: WidgetsCustom(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(' ${item.name}', style: const TextStyle(color: Colors.green)),
                                          Text(
                                            '${predictedStock.toStringAsFixed(2)} ${item.unit}',
                                            style: const TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 34.0),
                                          ),
                                        ],
                                      ),
                                      DropdownButton(
                                        isDense: true,
                                        value: actualDropdown,
                                        onChanged: (value) => setState(() {
                                          actualDropdown = value!;
                                          actualChart = chartDropdownItems.indexOf(value); // Refresh the chart
                                        }),
                                        items: chartDropdownItems.map(
                                          (String title) {
                                            return DropdownMenuItem(
                                              value: title,
                                              child: Text(title,
                                                  style: const TextStyle(color: Colors.blue, fontWeight: FontWeight.w400, fontSize: 14.0)),
                                            );
                                          },
                                        ).toList(),
                                      )
                                    ],
                                  ),
                                  const Padding(padding: EdgeInsets.only(bottom: 4.0)),
                                  Sparkline(
                                    data: charts[actualChart],
                                    pointsMode: PointsMode.all,
                                    averageLine: true,
                                    useCubicSmoothing: true,
                                    fillMode: FillMode.below,
                                    lineWidth: 3.0,
                                    lineColor: Colors.greenAccent,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  },
                );
        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Inventory Level For Next 7 Days Predicted!'),
            elevation: 2.0,
            backgroundColor: Colors.green,
          ),
        ),
        icon: const Icon(Icons.recycling_outlined),
        label: const Text('Predict Inventory'),
        backgroundColor: Colors.white60,
      ),
    );
  }
}

class InventoryItem {
  final String name;
  final double currentStock;
  final String unit;
  final List<double> salesHistory;

  InventoryItem(this.name, this.currentStock, this.salesHistory, this.unit);

  double predictInventory(double smoothingFactor, int forecastDays) {
    double predictedStock = currentStock;
    for (int i = 0; i < forecastDays; i++) {
      double forecast = salesHistory.last * smoothingFactor + (1 - smoothingFactor) * predictedStock;
      predictedStock = forecast;
    }
    return predictedStock;
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:text_form_field_wrapper/text_form_field_wrapper.dart';
import '../../../controllers/product_controller.dart';
import '../../drawer/drawer_widget_menu.dart';
import '../../widgets/form_input.dart';
import '../add_product.dart';

class ProductPrice extends StatefulWidget {
  const ProductPrice({super.key});

  @override
  State<ProductPrice> createState() => _ProductPriceState();
}

class _ProductPriceState extends State<ProductPrice> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        leading: const DrawerWidgetMenu(icon: FaIcon(FontAwesomeIcons.arrowLeft)),
        title: const Text(
          'Product Prices',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 30.0),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () => _pushScreen(
              context: context,
              screen: const AddProductScreen(),
            ),
          ),
        ],
      ),
      body: ValueListenableBuilder(
          valueListenable: Provider.of<ProductController>(context, listen: false).refreshNotifier,
          builder: (context, value, child) {
            return FutureBuilder(
              future: Provider.of<ProductController>(context, listen: false).initialize(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                return snapshot.connectionState == ConnectionState.waiting
                    ? const Center(child: CircularProgressIndicator())
                    : Consumer<ProductController>(
                        builder: (context, productController, child) {
                          return ListView.builder(
                            itemCount: productController.products.length,
                            itemBuilder: (context, index) {
                              var stocks = productController.stockMovements
                                  .where((element) => element.productId == productController.products[index].primaryKey)
                                  .toList();
                              double inStock = stocks
                                  .where((element) => element.stockMovement == 'stockIn')
                                  .map((e) => e.quantity)
                                  .fold(0, (a, b) => a + b);
                              double outStock = stocks
                                  .where((element) => element.stockMovement == 'stockOut')
                                  .map((e) => e.quantity)
                                  .fold(0, (a, b) => a + b);

                              double inStockTotalValue = stocks
                                  .where((element) => element.stockMovement == 'stockIn')
                                  .map((e) => (e.quantity * (e.buyingPrice / double.parse(e.rate))))
                                  .fold(0, (a, b) => a + b);

                              double outStockTotalValue = stocks
                                  .where((element) => element.stockMovement == 'stockOut')
                                  .map((e) => (e.quantity * (e.buyingPrice / double.parse(e.rate))))
                                  .fold(0, (a, b) => a + b);

                              //stock using average method
                              return InventoryCard(
                                itemName: productController.products[index].productName,
                                openingStock: double.parse(productController.products[index].quantity),
                                price: double.parse(productController.products[index].price),
                                inStock: inStock,
                                outStock: outStock,
                                closingStock: (double.parse(productController.products[index].quantity)) + inStock - outStock,
                                productId: productController.products[index].primaryKey!,
                              );
                            },
                          );
                        },
                      );
              },
            );
          }),
    );
  }
}

void _pushScreen({required BuildContext context, required Widget screen}) {
  ThemeData themeData = Theme.of(context);
  Navigator.push(
    context,
    MaterialPageRoute(builder: (_) => Theme(data: themeData, child: screen)),
  );
}

class InventoryCard extends StatefulWidget {
  final String itemName;
  final double openingStock;
  final double price;
  final double inStock;
  final double outStock;
  final double closingStock;
  final int productId;

  const InventoryCard({
    super.key,
    required this.itemName,
    required this.openingStock,
    required this.price,
    required this.inStock,
    required this.outStock,
    required this.closingStock,
    required this.productId,
  });

  @override
  State<InventoryCard> createState() => _InventoryCardState();
}

class _InventoryCardState extends State<InventoryCard> {
  TextEditingController amountNew = TextEditingController();

  late FocusNode productNameNode;

  @override
  void initState() {
    super.initState();
    productNameNode = FocusNode();
    amountNew.text = widget.price.toString();
  }

  showEditModal(context, productId) {
    return showDialog<String>(
      context: context,
      // barrierDismissible: false,
      builder: (BuildContext context) => Dialog(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(1.0)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                'Edit Sale Price ${widget.itemName}',
                style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.w200, color: Colors.blue),
              ),
              InputForm(
                icon: FontAwesomeIcons.moneyCheck,
                iconColor: Colors.blue,
                focusNode: productNameNode,
                controller: amountNew,
                inputType: TextInputType.number,
                prefix: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Text(
                        'Sale Price',
                        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.grey),
                      ),
                      Text(
                        'USD',
                        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w200, color: Colors.grey),
                      ),
                    ],
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('Close'),
                  ),
                  TextButton(
                    onPressed: () {
                      Provider.of<ProductController>(context, listen: false).updatePrice(
                        productId,
                        amountNew.text,
                        widget.itemName,
                        'productCode',
                      );
                      Navigator.pop(context);
                      productNameNode.unfocus();

                      //show snack bar
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          backgroundColor: Colors.green,
                          content: Text('Price Updated'),
                          duration: Duration(seconds: 2),
                        ),
                      );
                    },
                    child: const Text('Save Price'),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return InkWell(
      onTap: () => showEditModal(context, widget.productId),
      child: Card.filled(
        color: Colors.white,
        child: SizedBox(
          width: width,
          height: height / 5,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Row(
                  children: <Widget>[
                    Text(
                      "Click To Edit The Sale Price",
                      style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w200, color: Colors.red),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      widget.itemName,
                      style: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                    ),
                    Column(
                      children: [
                        const Text(
                          'Sale Price',
                          style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w200, color: Colors.grey),
                        ),
                        Text(
                          'USD ${widget.price.toStringAsFixed(2)}',
                          style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.blue),
                        ),
                      ],
                    ),
                  ],
                ),
                const Center(
                  child: Divider(
                    height: 10,
                    thickness: 0.8,
                    indent: 1,
                    endIndent: 1,
                    color: Colors.grey,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        const Text(
                          'Stock Value',
                          style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                        ),
                        Text(
                          '${widget.openingStock}',
                          style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                        ),
                      ],
                    ),
                    const VerticalDivider(
                      width: 20,
                      thickness: 1,
                      indent: 20,
                      endIndent: 0,
                      color: Colors.grey,
                    ),
                    Column(
                      children: [
                        const Text(
                          'Margin',
                          style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                        ),
                        Text(
                          '${widget.inStock}',
                          style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                        ),
                      ],
                    ),
                    const VerticalDivider(
                      width: 20,
                      thickness: 1,
                      indent: 20,
                      endIndent: 0,
                      color: Colors.grey,
                    ),
                    Column(
                      children: [
                        const Text(
                          'Margin %',
                          style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                        ),
                        Text(
                          '${widget.outStock}',
                          style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                        ),
                      ],
                    ),
                    const VerticalDivider(
                      width: 20,
                      thickness: 1,
                      indent: 20,
                      endIndent: 0,
                      color: Colors.grey,
                    ),
                    Column(
                      children: [
                        const Text(
                          'Closing Stock',
                          style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                        ),
                        Text(
                          '${widget.closingStock}',
                          style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

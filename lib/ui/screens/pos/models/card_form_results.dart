/// Class that houses all expected form data from the checkout ui
///
/// !DO NOT SAVE THIS DATA TO A LOG FILE OR PRINT IT OUT IN LIVE CODE
class CardFormResults {
  /// Class that houses all expected form data from the checkout ui
  const CardFormResults({
    required this.currency,
    required this.rate,
    required this.nameCustomer,
    required this.customerPhone,
    required this.amount,
    this.status = 'active',
  });


  /// email string
  final String currency;

  /// 15 to 16 digit credit card number. numeric characters only and no spaces
  final String rate;

  /// full expiration date string MM/YY format
  final String nameCustomer;

  /// cvv string. numeric characters only and no spaces
  final String customerPhone;

  /// string representing buyer's name
  final String amount;

  /// 10 digit phone number. numeric characters only and no spaces
  final String status;

  /// getter to retrieve the year from expiration string
  int get expYear => int.parse(nameCustomer.split('/')[1]) + 2000;

  /// getter to retrieve the expiration month from the expiration string
  int get expMonth => int.parse(nameCustomer.split('/')[0]);

  @override
  String toString() {
    return 'CardFormResults [$currency , $rate, $nameCustomer, $customerPhone, $amount, $status]';
  }
}

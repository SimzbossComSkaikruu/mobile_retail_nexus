import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import '../../../controllers/cart.dart';
import '../../../controllers/product_controller.dart';
import '../../../data/local/product.model.dart';
import '../../drawer/drawer_widget_menu.dart';
import '../add_product.dart';
import 'cart.dart';

class PointOfSaleIndex extends StatefulWidget {
  const PointOfSaleIndex({super.key});

  @override
  State<PointOfSaleIndex> createState() => _PointOfSaleIndexState();
}

class _PointOfSaleIndexState extends State<PointOfSaleIndex> {
  _addProductCart(ProductModel product) {
    Provider.of<CartController>(context, listen: false).addToCart(product);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        elevation: 2.0,
        backgroundColor: Colors.white,
        leading: const DrawerWidgetMenu(icon: FaIcon(FontAwesomeIcons.arrowLeft)),
        title: const Text('New Sale', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 30.0)),
        actions: [
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: () => _pushScreen(
              context: context,
              screen: const AddProductScreen(),
            ),
          ),
        ],
      ),
      body: ValueListenableBuilder(
        valueListenable: Provider.of<CartController>(context, listen: false).refreshNotifier,
        builder: (context, value, child) {
          var cartController = Provider.of<CartController>(context, listen: false);
          return Column(
            children: [
              Expanded(
                child: FutureBuilder(
                  future: Provider.of<ProductController>(context, listen: false).initialize(),
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    return snapshot.connectionState == ConnectionState.waiting
                        ? const Center(child: CircularProgressIndicator())
                        : Consumer<ProductController>(
                            builder: (context, productController, child) {
                              return ListView.builder(
                                itemCount: productController.products.length,
                                itemBuilder: (context, index) {
                                  var stocks = productController.stockMovements
                                      .where((element) => element.productId == productController.products[index].primaryKey)
                                      .toList();
                                  double inStock = stocks
                                      .where((element) => element.stockMovement == 'stockIn')
                                      .map((e) => e.quantity)
                                      .fold(0, (a, b) => a + b);
                                  double outStock = stocks
                                      .where((element) => element.stockMovement == 'stockOut')
                                      .map((e) => e.quantity)
                                      .fold(0, (a, b) => a + b);

                                  //stock using average method
                                  return InkWell(
                                    onTap: () => _addProductCart(productController.products[index]),
                                    child: InventoryCard(
                                      itemName: productController.products[index].productName,
                                      openingStock: double.parse(productController.products[index].quantity),
                                      price: double.parse(productController.products[index].price),
                                      inStock: inStock,
                                      outStock: outStock,
                                      closingStock: (double.parse(productController.products[index].quantity)) + inStock - outStock,
                                      productId: productController.products[index].primaryKey!,
                                      product: productController.products[index],
                                    ),
                                  );
                                },
                              );
                            },
                          );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '${cartController.cartCount} Items in Cart',
                          style: Theme.of(context).textTheme.bodySmall,
                        ),
                        Text(
                          'USD ${cartController.totalAmount}',
                          style: Theme.of(context).textTheme.titleSmall,
                        ),
                      ],
                    ),
                    CallToActionButton(
                      onPressed: () => _pushScreen(
                        context: context,
                        screen: const CartScreen(),
                      ),
                      labelText: 'Next',
                      minSize: const Size(220, 45),
                      foregroundColor: Colors.blue,
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 16),
            ],
          );
        },
      ),
    );
  }
}

class InventoryCard extends StatefulWidget {
  final String itemName;
  final double openingStock;
  final double price;
  final double inStock;
  final double outStock;
  final double closingStock;
  final int productId;
  final ProductModel product;

  const InventoryCard(
      {super.key,
      required this.itemName,
      required this.openingStock,
      required this.price,
      required this.inStock,
      required this.outStock,
      required this.closingStock,
      required this.productId,
      required this.product});

  @override
  State<InventoryCard> createState() => _InventoryCardState();
}

class _InventoryCardState extends State<InventoryCard> {
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Card.filled(
      color: Colors.white,
      child: SizedBox(
        width: width,
        height: 100,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    widget.itemName,
                    style: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                  ),
                  Column(
                    children: [
                      const Text(
                        'Sale Price',
                        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w200, color: Colors.grey),
                      ),
                      Text(
                        'USD ${widget.price.toStringAsFixed(2)}',
                        style: const TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold, color: Colors.blue),
                      ),
                    ],
                  ),
                ],
              ),
              const Center(
                child: Divider(
                  height: 10,
                  thickness: 0.8,
                  indent: 1,
                  endIndent: 1,
                  color: Colors.grey,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      const Text(
                        'Stock Value',
                        style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                      ),
                      Text(
                        '${widget.openingStock}',
                        style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                      ),
                    ],
                  ),
                  const VerticalDivider(
                    width: 20,
                    thickness: 1,
                    indent: 20,
                    endIndent: 0,
                    color: Colors.grey,
                  ),
                  Column(
                    children: [
                      const Text(
                        'Margin',
                        style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                      ),
                      Text(
                        '${widget.inStock}',
                        style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                      ),
                    ],
                  ),
                  const VerticalDivider(
                    width: 20,
                    thickness: 1,
                    indent: 20,
                    endIndent: 0,
                    color: Colors.grey,
                  ),
                  Column(
                    children: [
                      const Text(
                        'Margin %',
                        style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                      ),
                      Text(
                        '${widget.outStock}',
                        style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                      ),
                    ],
                  ),
                  const VerticalDivider(
                    width: 20,
                    thickness: 1,
                    indent: 20,
                    endIndent: 0,
                    color: Colors.grey,
                  ),
                  Column(
                    children: [
                      const Text(
                        'Closing Stock',
                        style: TextStyle(fontSize: 9.0, fontWeight: FontWeight.w400, color: Colors.grey),
                      ),
                      Text(
                        '${widget.closingStock}',
                        style: const TextStyle(fontSize: 8.0, fontWeight: FontWeight.bold, color: Colors.blue),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CallToActionButton extends StatelessWidget {
  const CallToActionButton({
    required this.onPressed,
    required this.labelText,
    this.minSize = const Size(266, 45),
    super.key,
    this.backgroundColor,
    this.foregroundColor,
  });

  final Function onPressed;
  final String labelText;
  final Size minSize;
  final Color? backgroundColor;
  final Color? foregroundColor;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed as void Function()?,
      style: ElevatedButton.styleFrom(
        minimumSize: minSize,
        backgroundColor: backgroundColor,
        foregroundColor: foregroundColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(14),
        ),
      ),
      child: Text(
        labelText,
        style: const TextStyle(
          fontSize: 16,
        ),
      ),
    );
  }
}

void _pushScreen({required BuildContext context, required Widget screen}) {
  ThemeData themeData = Theme.of(context);
  Navigator.push(
    context,
    MaterialPageRoute(
      builder: (_) => Theme(data: themeData, child: screen),
    ),
  );
}

class SearchBar extends StatefulWidget {
  const SearchBar({required this.onChanged, super.key});

  final Function(String) onChanged;

  @override
  State<SearchBar> createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  late TextEditingController _textEditingController;

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColorLight,
        borderRadius: BorderRadius.circular(18),
      ),
      child: TextField(
        onChanged: widget.onChanged,
        controller: _textEditingController,
        textAlignVertical: TextAlignVertical.center,
        textInputAction: TextInputAction.search,
        decoration: InputDecoration(
          border: InputBorder.none,
          isDense: true,
          contentPadding: EdgeInsets.zero,
          prefixIconConstraints: const BoxConstraints(
            minHeight: 36,
            minWidth: 36,
          ),
          prefixIcon: const Icon(
            Icons.search,
          ),
          hintText: 'Search for a product',
          suffixIconConstraints: const BoxConstraints(
            minHeight: 36,
            minWidth: 36,
          ),
          suffixIcon: IconButton(
            constraints: const BoxConstraints(
              minHeight: 36,
              minWidth: 36,
            ),
            splashRadius: 24,
            icon: const Icon(
              Icons.clear,
            ),
            onPressed: () {
              _textEditingController.clear();
              widget.onChanged(_textEditingController.text);
              FocusScope.of(context).unfocus();
            },
          ),
        ),
      ),
    );
  }
}

part of 'checkout_page.dart';

class CheckoutData {
  /// The CheckoutData widget is a stateless widget resembling your typical
  /// checkout page and some typical option along with some helpful features
  /// such as built-in form validation and credit card icons that update
  /// based on the input provided.
  /// This is a UI widget only and holds no responsibility and makes no guarantee
  /// for transactions using this ui. Transaction security and integrity is
  /// the responsibility of the developer and what ever Third-party transaction
  /// api that developer is using. A great API to use is Stripe
  const CheckoutData(
      {required this.priceItems,
      required this.payToName,
      this.displayNativePay = false,
      this.isApple = false,
      this.displayEmail = true,
      this.lockEmail = false,
      this.initEmail = '',
      this.initPhone = '',
      this.initBuyerName = '',
      this.countriesOverride,
      this.onBack,
      this.payBtnKey,
      this.formKey,
      this.cashPrice,
      this.taxRate,
      this.displayTestData = false});

  /// The list of items with prices [PriceItem]'s to be shown within the
  /// drop down banner on the checkout page
  final List<PriceItem> priceItems;

  /// If you are providing a cash option at a discount, provide its price
  /// ex: 12.99
  final double? cashPrice;

  /// Provide the name of the vendor handling the transaction or receiving the
  /// funds from the user during this transaction
  final String payToName;

  /// should you display native pay option?
  final bool displayNativePay;

  /// is this the user on an apple based platform?
  final bool isApple;

  /// Should the email box be displayed?
  final bool displayEmail;

  /// Should the email form field be locked? This should only be done if an
  /// [initEmail] is provided
  final bool lockEmail;

  /// Provide an email if you have it, to prefill the email field on the Credit
  /// Card form
  final String initEmail;

  /// Provide a name if you have it, to prefill the name field on the Credit
  /// Card form
  final String initBuyerName;

  /// Provide a phone number if you have it, to prefill the name field on the
  /// Credit Card form
  final String initPhone;

  /// If you have a List of Countries that you would like to use to override the
  /// currently provide list of 1, being 'United States', add the list here.
  /// Warning: The credit card form does not currently adjust based on selected
  /// country's needs to verify a card. This form may not work for all countries
  final List<String>? countriesOverride;

  /// If you would like to provide an integrated back button in the header, add
  /// add the needed functionality here.
  /// ex) onBack : ()=>Navigator.of(context).pop();
  final Function? onBack;

  /// If you would like to control the pay button state to display text or icons
  /// based on the current stage of the payment process, you will need to
  /// provide a [CardPayButtonState] key to update it.
  final GlobalKey<CardPayButtonState>? payBtnKey;

  /// You will need to provide a general [FormState] key to control, validate
  /// and save the form data based on your needs.
  final GlobalKey<FormState>? formKey;

  /// If you would like to display test data during your development, a dataset
  /// based on Stripe test data is provided. To use this date, simply mark this
  /// true.
  /// WARNING: Make sure to mark false for any release
  final bool displayTestData;

  /// The tax rate to be applied to the total price
  ///
  /// default is null for no tax
  ///
  /// examples:
  ///  - 0.07 for 7%
  ///  - 0.20 for 20%
  ///  - null for no tax
  ///  - 5.00 for 500%
  final double? taxRate;

  int get totalTaxCents {
    if (taxRate == null) {
      return 0;
    }
    return (totalCostCents * taxRate!).toInt();
  }

  String get totalTax => (totalTaxCents.toDouble()).toStringAsFixed(2);

  double get subtotalCents {
    return priceItems.fold(0, (prev, item) => prev + item.totalPriceCents);
  }

  double get totalCostCents {
    double amount = priceItems.fold(0, (prev, item) => prev + item.totalPriceCents);
    if (taxRate != null) {
      amount += (amount * taxRate!).toInt();
    }
    return amount;
  }

  String get totalPrice => (totalCostCents.toDouble()).toStringAsFixed(2);

}

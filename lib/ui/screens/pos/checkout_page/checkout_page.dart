import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../credit_form/credit_card_form.dart';
import '../models/price_item.dart';
import '../ui_components/pay_button.dart';

part 'price_list_item.dart';

part 'checkout_data.dart';

part 'checkout_view_model.dart';

part 'charge_amount_display.dart';

part 'tax_item.dart';

part 'list_of_charges.dart';

/// The CheckoutPage widget is a stateless widget resembling your typical
/// checkout page and some typical option along with some helpful features
/// such as built-in form validation and credit card icons that update
/// based on the input provided.
/// This is a UI widget only and holds no responsibility and makes no guarantee
/// for transactions using this ui. Transaction security and integrity is
/// the responsibility of the developer and what ever Third-party transaction
/// api that developer is using. A great API to use is Stripe
class CheckoutPage extends StatelessWidget {
  /// The CheckoutPage widget is a stateless widget resembling your typical
  /// checkout page and some typical option along with some helpful features
  /// such as built-in form validation and credit card icons that update
  /// based on the input provided.
  /// This is a UI widget only and holds no responsibility and makes no guarantee
  /// for transactions using this ui. Transaction security and integrity is
  /// the responsibility of the developer and what ever Third-party transaction
  /// api that developer is using. A great API to use is Stripe
  CheckoutPage({super.key, required this.data, this.footer}) : _viewModel = _CheckoutViewModel(data: data);

  final CheckoutData data;

  final _CheckoutViewModel _viewModel;

  /// Provide a footer to end the checkout page using any desired widget or
  /// use our built-in [CheckoutPageFooter]
  final Widget? footer;

  @override
  Widget build(BuildContext context) {
    // some ui sizing variables
    const double spacing = 30.0;
    const double collapsedAppBarHeight = 100;

    // calculate the height of the expanded appbar based on the total number
    // of line items to display.
    final double expHeight = (data.priceItems.length > 10 ? 500 : data.priceItems.length * 50) + (data.taxRate != null ? 50 : 0) + 165;

    // Calculate the init height the scroll should be set to to properly
    // display the title and amount to be charged
    final double initHeight = expHeight - (collapsedAppBarHeight + 30.0);

    // create a ScrollController to listen to whether or not the appbar is open
    _viewModel.setScrollController(ScrollController(initialScrollOffset: initHeight));

    return CustomScrollView(
      controller: _viewModel.scrollController,
      slivers: [
        SliverAppBar(
          snap: false,
          pinned: false,
          floating: false,
          backgroundColor: Colors.grey.shade50,
          collapsedHeight: collapsedAppBarHeight,
          // set to false to prevent undesired back arrow
          automaticallyImplyLeading: false,
          title: Row(
            children: [
              SizedBox(
                width: 40,
                child: (data.onBack != null)
                    ? IconButton(
                        onPressed: () => data.onBack!(),
                        icon: const FaIcon(FontAwesomeIcons.arrowLeft),
                      )
                    : null,
              ),
              Expanded(
                  child: Text(
                data.payToName.length < 16 ? data.payToName : data.payToName,
                textAlign: TextAlign.center,
                style: const TextStyle(fontSize: 26, color: Colors.black, fontWeight: FontWeight.bold),
              )),
              const SizedBox(
                width: 40,
              ),
            ],
          ),
          bottom: PreferredSize(
            preferredSize: const Size(120.0, 32.0),
            child: _ChargeAmountDisplay(key: _viewModel.totalDisplayKey, initHeight: initHeight, viewModel: _viewModel),
          ),
          expandedHeight: expHeight,
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: true,
            background: Padding(
              padding: const EdgeInsets.fromLTRB(16.0, 80, 16.0, 0),
              child: _ListOfCharges(viewModel: _viewModel),
            ),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate(
            [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  children: [
                    const SizedBox(height: spacing),
                    CreditCardForm(
                      amount: double.parse(data.totalPrice),
                      formKey: data.formKey,
                      payBtnKey: data.payBtnKey,
                    ),
                    footer ?? const SizedBox(height: 120),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

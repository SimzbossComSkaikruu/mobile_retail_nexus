import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:retail/ui/screens/pos/point_of_sale.index.dart';
import 'package:retail/ui/screens/pos/process.payment.dart';
import '../../../controllers/cart.dart';
import '../../drawer/drawer_widget_menu.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({super.key});

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  void updateState() => setState(() {});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        foregroundColor: Colors.white,
        elevation: 0,
        leading: const DrawerWidgetMenu(icon: FaIcon(FontAwesomeIcons.arrowLeft, color: Colors.white)),
        centerTitle: true,
        title: const Column(
          children: [
            Text('Cart', style: TextStyle(fontWeight: FontWeight.bold)),
            Text(
              'Sum items in cart',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.normal,
              ),
            )
          ],
        ),
      ),
      body: ValueListenableBuilder(
          valueListenable: Provider.of<CartController>(context, listen: false).refreshNotifier,
          builder: (context, value, child) {
            var cartController = Provider.of<CartController>(context, listen: false);
            return Column(
              children: [
                Expanded(
                  child: FutureBuilder(
                    future: null,
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      return snapshot.connectionState == ConnectionState.waiting
                          ? const Center(child: CircularProgressIndicator())
                          : Consumer<CartController>(
                              builder: (context, cartController, child) {
                                return ListView.builder(
                                  itemCount: cartController.uniqueProducts.length,
                                  itemBuilder: (context, index) {
                                    return Column(
                                      children: [
                                        const SizedBox(
                                          width: 16,
                                        ),
                                        Row(
                                          children: [
                                            const SizedBox(
                                              width: 16,
                                            ),
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    cartController.uniqueProducts[index].product.productName,
                                                    style: Theme.of(context).textTheme.titleMedium,
                                                  ),
                                                  const SizedBox(
                                                    height: 8,
                                                  ),
                                                  Row(
                                                    children: [
                                                      Text(
                                                        '${cartController.uniqueProducts[index].quantityProduct} items @ ${cartController.uniqueProducts[index].product.price}',
                                                        style: Theme.of(context).textTheme.bodySmall,
                                                      ),
                                                      const SizedBox(
                                                        width: 16,
                                                      ),
                                                      Text(
                                                        '\$ ${cartController.uniqueProducts[index].quantityProduct * double.parse(cartController.uniqueProducts[index].product.price)}',
                                                        style: Theme.of(context).textTheme.titleSmall,
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                            IconButton(
                                              icon: const Icon(Icons.add),
                                              onPressed: () => cartController.addToCart(cartController.uniqueProducts[index].product),
                                              color: Colors.blue,
                                            ),
                                            IconButton(
                                              icon: const Icon(Icons.remove),
                                              onPressed: () => cartController.removeFromCart(cartController.uniqueProducts[index].product),
                                              color: Colors.green,
                                            ),
                                            IconButton(
                                              icon: const Icon(Icons.close),
                                              onPressed: () => cartController.removeProduct(cartController.uniqueProducts[index].product),
                                              color: Colors.red,
                                            ),
                                          ],
                                        ),
                                        const Center(
                                          child: Divider(
                                            height: 10,
                                            thickness: 0.8,
                                            indent: 1,
                                            endIndent: 1,
                                            color: Colors.grey,
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              },
                            );
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${cartController.cartCount} Items in Cart',
                            style: Theme.of(context).textTheme.bodySmall,
                          ),
                          Text(
                            '\$ ${cartController.totalAmount}',
                            style: Theme.of(context).textTheme.titleSmall,
                          ),
                        ],
                      ),

                      //process payment
                      CallToActionButton(
                        onPressed: () => _pushScreen(context: context, screen: const ProcessPayment()),
                        labelText: 'Check Out',
                        minSize: const Size(220, 45),
                        backgroundColor: Colors.blue,
                        foregroundColor: Colors.white,
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 16),
              ],
            );
          }),
    );
  }
}

void _pushScreen({required BuildContext context, required Widget screen}) {
  ThemeData themeData = Theme.of(context);
  Navigator.push(
    context,
    MaterialPageRoute(
      builder: (_) => Theme(data: themeData, child: screen),
    ),
  );
}

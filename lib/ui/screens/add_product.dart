import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import '../../controllers/product_controller.dart';
import '../../data/local/product.model.dart';
import '../drawer/drawer_widget_menu.dart';
import '../widgets/date_input.dart';
import '../widgets/dropdown.dart';
import '../widgets/form_input.dart';
import 'inventory/add_inventory.dart';

class AddProductScreen extends StatelessWidget {
  const AddProductScreen({super.key});

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          elevation: 2.0,
          backgroundColor: Colors.white,
          leading: const DrawerWidgetMenu(
            icon: FaIcon(FontAwesomeIcons.arrowLeft),
          ),
          title: const Text(
            'Add Item',
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.w700,
                fontSize: 30.0),
          ),
        ),
        body: SafeArea(
          child: Card(
            color: Colors.white,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(1.0)),
            child: const AddProductForm(),
          ),
        ),
      );
}

class AddProductForm extends StatefulWidget {
  const AddProductForm({super.key});

  @override
  State<AddProductForm> createState() => _AddProductFormState();
}

class _AddProductFormState extends State<AddProductForm> {
  final _formKey = GlobalKey<FormState>();
  late FocusNode productNameNode;
  late FocusNode productCodeNode;
  late FocusNode productCategoryNode;
  late FocusNode measurementUnitNode;
  late FocusNode descriptionNode;
  late FocusNode priceNode;
  late FocusNode purchasePriceNode;
  late FocusNode quantityNode;
  late FocusNode minimumQuantityNode;
  late FocusNode openingDate;
  late FocusNode buyingRateFocusNode;

  final productNameController = TextEditingController();
  final productCodeController = TextEditingController();
  final productCategoryController = TextEditingController();
  final measurementUnitController = TextEditingController();
  final descriptionController = TextEditingController();
  final priceController = TextEditingController();
  final purchasePriceController = TextEditingController();
  final quantityController = TextEditingController();
  final minimumQuantityController = TextEditingController();
  final openingDateController = TextEditingController();
  final currencyController = TextEditingController();
  final buyingRateController = TextEditingController();

  @override
  void initState() {
    super.initState();
    productNameNode = FocusNode();
    productCodeNode = FocusNode();
    productCategoryNode = FocusNode();
    measurementUnitNode = FocusNode();
    descriptionNode = FocusNode();
    priceNode = FocusNode();
    quantityNode = FocusNode();
    minimumQuantityNode = FocusNode();
    openingDate = FocusNode();
    purchasePriceNode = FocusNode();
    buyingRateFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    productNameNode.dispose();
    productCodeNode.dispose();
    productCategoryNode.dispose();
    measurementUnitNode.dispose();
    descriptionNode.dispose();
    priceNode.dispose();
    quantityNode.dispose();
    minimumQuantityNode.dispose();
    openingDate.dispose();
    purchasePriceNode.dispose();
    productNameController.dispose();
    productCodeController.dispose();
    productCategoryController.dispose();
    measurementUnitController.dispose();
    descriptionController.dispose();
    priceController.dispose();
    purchasePriceController.dispose();
    quantityController.dispose();
    minimumQuantityController.dispose();
    openingDateController.dispose();
    currencyController.dispose();
    buyingRateController.dispose();
    super.dispose();
  }

  ButtonStyle get _elevatedButtonStyle => ButtonStyle(
        textStyle: MaterialStateProperty.all<TextStyle>(
          const TextStyle(color: Colors.white),
        ),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(1.0),
          ),
        ),
        backgroundColor: MaterialStateProperty.all(Colors.blue),
      );

  @override
  Widget build(BuildContext context) {
    var hintText = 'kg , ltr , pcs';
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: LayoutBuilder(
            builder:
                (BuildContext context, BoxConstraints viewportConstraints) {
              return SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child: ConstrainedBox(
                  constraints:
                      BoxConstraints(minHeight: viewportConstraints.maxHeight),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InputForm(
                        label: 'Product/Service Name',
                        labelStyle: const TextStyle(
                            fontStyle: FontStyle.normal,
                            color: Colors.blue,
                            fontWeight: FontWeight.w300),
                        icon: FontAwesomeIcons.bagShopping,
                        iconColor: Colors.blue,
                        focusNode: productNameNode,
                        controller: productNameController,
                      ),
                      InputForm(
                        label: 'Product/Service Code',
                        labelStyle: const TextStyle(
                            fontStyle: FontStyle.normal,
                            color: Colors.blue,
                            fontWeight: FontWeight.w300),
                        icon: FontAwesomeIcons.bagShopping,
                        focusNode: productCodeNode,
                        controller: productCodeController,
                        iconColor: Colors.blue,
                      ),
                      InputForm(
                        label: 'Product/Service Category',
                        labelStyle: const TextStyle(
                            fontStyle: FontStyle.normal,
                            color: Colors.blue,
                            fontWeight: FontWeight.w300),
                        icon: FontAwesomeIcons.bagShopping,
                        iconColor: Colors.blue,
                        focusNode: productCategoryNode,
                        controller: productCategoryController,
                      ),
                      InputForm(
                        label: 'Measurement Unit',
                        labelStyle: const TextStyle(
                            fontStyle: FontStyle.normal,
                            color: Colors.blue,
                            fontWeight: FontWeight.w300),
                        icon: FontAwesomeIcons.toolbox,
                        iconColor: Colors.blue,
                        focusNode: measurementUnitNode,
                        hintText: hintText,
                        hintStyle: const TextStyle(color: Colors.blue),
                        isUpperCase: true,
                        controller: measurementUnitController,
                      ),
                      InputForm(
                        label: 'Product Description',
                        labelStyle: const TextStyle(
                            fontStyle: FontStyle.normal,
                            color: Colors.blue,
                            fontWeight: FontWeight.w300),
                        icon: FontAwesomeIcons.alignJustify,
                        iconColor: Colors.blue,
                        focusNode: descriptionNode,
                        controller: descriptionController,
                      ),
                      InputForm(
                        label: 'Opening Quantity',
                        labelStyle: const TextStyle(
                            fontStyle: FontStyle.normal,
                            color: Colors.blue,
                            fontWeight: FontWeight.w300),
                        icon: FontAwesomeIcons.archive,
                        iconColor: Colors.blue,
                        focusNode: quantityNode,
                        inputType: TextInputType.number,
                        controller: quantityController,
                      ),
                      InputForm(
                          label: 'Selling Price',
                          labelStyle: const TextStyle(
                              fontStyle: FontStyle.normal,
                              color: Colors.blue,
                              fontWeight: FontWeight.w300),
                          icon: FontAwesomeIcons.moneyCheckDollar,
                          iconColor: Colors.blue,
                          focusNode: priceNode,
                          inputType: TextInputType.number,
                          controller: priceController),
                      InputForm(
                        label: 'Purchase Price',
                        labelStyle: const TextStyle(
                            fontStyle: FontStyle.normal,
                            color: Colors.blue,
                            fontWeight: FontWeight.w300),
                        icon: FontAwesomeIcons.moneyCheckDollar,
                        iconColor: Colors.blue,
                        focusNode: purchasePriceNode,
                        inputType: TextInputType.number,
                        controller: purchasePriceController,
                      ),
                      CustomDropDown<IconLabel>(
                        items: IconLabel.values
                            .map<DropdownMenuEntry<IconLabel>>(
                              (IconLabel icon) => DropdownMenuEntry<IconLabel>(
                                style: ButtonStyle(
                                  textStyle:
                                      MaterialStateProperty.all<TextStyle>(
                                    const TextStyle(
                                        fontStyle: FontStyle.normal,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w300),
                                  ),
                                ),
                                value: icon,
                                label: icon.label,
                                leadingIcon: Text(
                                  CurrencyUtil().currencyEmoji(icon.icon),
                                  style: const TextStyle(
                                      fontStyle: FontStyle.normal,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w300),
                                ),
                              ),
                            )
                            .toList(),
                        controller: currencyController,
                        label: const Text("Select Currency"),
                        leadingIcon:
                            const Icon(FontAwesomeIcons.moneyCheckDollar),
                      ),
                      InputForm(
                        label: 'Buying Rate Against 1 USD',
                        prefix: const Icon(FontAwesomeIcons.dollarSign),
                        labelStyle: const TextStyle(
                            fontStyle: FontStyle.normal,
                            color: Colors.black,
                            fontWeight: FontWeight.w300),
                        focusNode: buyingRateFocusNode,
                        inputType: TextInputType.number,
                        controller: buyingRateController,
                        contentPadding: 1.0,
                        border: const OutlineInputBorder(
                          borderSide:
                              BorderSide(color: Colors.black, width: 1.0),
                          borderRadius: BorderRadius.all(Radius.circular(1.0)),
                          gapPadding: 2.0,
                        ),
                      ),
                      InputForm(
                        label: 'Minimum Quantity Alert',
                        labelStyle: const TextStyle(
                            fontStyle: FontStyle.normal,
                            color: Colors.blue,
                            fontWeight: FontWeight.w300),
                        icon: FontAwesomeIcons.bell,
                        iconColor: Colors.blue,
                        focusNode: openingDate,
                        inputType: TextInputType.number,
                        controller: minimumQuantityController,
                      ),
                      DateInput(
                        label: 'Opening Date',
                        labelStyle: const TextStyle(
                            fontStyle: FontStyle.normal,
                            color: Colors.blue,
                            fontWeight: FontWeight.w300),
                        icon: FontAwesomeIcons.calendarDay,
                        controller: openingDateController,
                        iconColorS: Colors.blue,
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 16),
                              child: ElevatedButton(
                                style: _elevatedButtonStyle,
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: const Text('Cancel',
                                    style: TextStyle(color: Colors.black)),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 16),
                              child: ElevatedButton(
                                style: _elevatedButtonStyle,
                                onPressed: () {
                                  // Validate returns true if the form is valid, or false otherwise.
                                  if (_formKey.currentState!.validate()) {
                                    // If the form is valid, display a snack-bar. In the real world,
                                    // you'd often call a server or save the information in a database.
                                    var productName =
                                        productNameController.text;
                                    var productCode =
                                        productCodeController.text;
                                    var productCategory =
                                        productCategoryController.text;
                                    var measurementUnit =
                                        measurementUnitController.text;
                                    var description =
                                        descriptionController.text;
                                    var price = priceController.text;
                                    var purchasePrice =
                                        purchasePriceController.text;
                                    var quantity = quantityController.text;
                                    var minimumQuantity =
                                        minimumQuantityController.text;
                                    var openingDate =
                                        openingDateController.text;

                                    //create a product object
                                    ProductModel product = ProductModel(
                                      productName: productName,
                                      productCode: productCode,
                                      productCategory: productCategory,
                                      measurementUnit: measurementUnit,
                                      description: description,
                                      price: price,
                                      purchasePrice: purchasePrice,
                                      quantity: quantity,
                                      minimumQuantity: minimumQuantity,
                                      openingDate: openingDate,
                                      isStocked: 'true',
                                      currency: currencyController.text,
                                      rate: double.parse(
                                          buyingRateController.text),
                                    );

                                    //save to database
                                    ProductController();

                                    //add product to stock
                                    Provider.of<ProductController>(context,
                                            listen: false)
                                        .addProduct(product);

                                    //clear the form
                                    productNameController.clear();
                                    productCodeController.clear();
                                    productCategoryController.clear();
                                    measurementUnitController.clear();
                                    descriptionController.clear();
                                    priceController.clear();
                                    purchasePriceController.clear();
                                    quantityController.clear();
                                    minimumQuantityController.clear();
                                    openingDateController.clear();
                                    currencyController.clear();
                                    buyingRateController.clear();

                                    //show snack-bar
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      const SnackBar(
                                        content:
                                            Text('Data Saved Successfully'),
                                        elevation: 2.0,
                                        backgroundColor: Colors.green,
                                      ),
                                    );

                                    // Navigator.pop(context);
                                    // Navigator.push(
                                    //   context,
                                    //   MaterialPageRoute(builder: (context) => const InventoryStatus()),
                                    // );
                                  }
                                },
                                child: const Text('Submit',
                                    style: TextStyle(color: Colors.black)),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import '../components/configuration/constants.dart';
import '../components/effects/shimmer.dart';

class WidgetsCustom extends StatelessWidget {
  final Widget child;
  final Color? color;
  const WidgetsCustom({super.key, required this.child, this.color});

  @override
  Widget build(BuildContext context) => Material(
      color: (color != null) ? color : Colors.white,
      elevation: 14.0,
      borderRadius: BorderRadius.circular(12.0),
      shadowColor: const Color(0x802196F3),
      child: child);
}

class CardListItem extends StatelessWidget {
  const CardListItem({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(6.0),
      child: _buildCardPlaceHolder(),
    );
  }

  Widget _buildCardPlaceHolder() {
    return AspectRatio(
      aspectRatio: 2.5,
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(16),
        ),
      ),
    );
  }
}

class LoadingAnimation extends StatefulWidget {
  final bool isLoading;
  final double height;

  const LoadingAnimation({
    super.key,
    required this.isLoading,
    required this.height,
  });

  @override
  State<LoadingAnimation> createState() => _LoadingAnimationState();
}

class _LoadingAnimationState extends State<LoadingAnimation> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Shimmer(
        linearGradient: shimmerWhiteGradient,
        child: ListView(
          // physics:
          //     widget.isLoading ? null : const NeverScrollableScrollPhysics(),
          children: [
            SizedBox(height: (widget.height * 0.19)),
            _buildListItem(),
            _buildListItem(),
            _buildListItem(),
            _buildListItem(),
            _buildListItem(),
          ],
        ),
      ),
    );
  }

  Widget _buildListItem() {
    return ShimmerLoading(
      isLoading: widget.isLoading,
      child: const CardListItem(),
    );
  }
}


import 'package:flutter/material.dart';

class CustomDropDown<T> extends StatefulWidget {
  const CustomDropDown(
      {super.key,
      required this.items,
      this.onSelected,
      this.inputDecorationTheme,
      this.label,
      required this.controller,
      this.width,
      this.leadingIcon});

  final List<DropdownMenuEntry<T>> items;
  final void Function(T?)? onSelected;
  final InputDecorationTheme? inputDecorationTheme;
  final Widget? label;
  final TextEditingController controller;
  final double? width;
  final Widget? leadingIcon;

  @override
  State<CustomDropDown<T>> createState() => _CustomDropDownState();
}

class _CustomDropDownState<T> extends State<CustomDropDown<T>> {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return DropdownMenu<T>(
      inputDecorationTheme: widget.inputDecorationTheme ??
          const InputDecorationTheme(
            labelStyle: TextStyle(
                fontStyle: FontStyle.normal,
                color: Colors.black,
                fontWeight: FontWeight.w300),
            contentPadding: EdgeInsets.all(1.0),
            activeIndicatorBorder: BorderSide(color: Colors.blue, width: 2.0),
            // outlineBorder: BorderSide(color: Colors.black, width: 1.0),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black, width: 2.0),
              borderRadius: BorderRadius.all(Radius.circular(1.0)),
              gapPadding: 1.0,
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black, width: 2.0),
              borderRadius: BorderRadius.all(Radius.circular(1.0)),
              gapPadding: 1.0,
            ),
          ),
      width: widget.width ?? (width * 0.92),
      enableFilter: true,
      // requestFocusOnTap: true,
      textStyle: const TextStyle(
          fontStyle: FontStyle.normal,
          color: Colors.black,
          fontWeight: FontWeight.w300),
      leadingIcon: widget.leadingIcon,
      controller: widget.controller,
      label: widget.label,
      onSelected: widget.onSelected,
      dropdownMenuEntries: widget.items,
    );
  }
}

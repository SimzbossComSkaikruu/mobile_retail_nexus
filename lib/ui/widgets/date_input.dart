import 'package:flutter/material.dart';

class DateInput extends StatefulWidget {
  const DateInput(
      {super.key,
      this.restorationId,
      this.label,
      this.icon,
      this.focusNode,
      this.inputType,
      this.hintText,
      required this.controller,
      this.isUpperCase,
      this.width,
      this.contentPadding,
      this.iconColorS,
      this.border,
      this.labelStyle});

  final String? restorationId;
  final String? label;
  final IconData? icon;
  final Color? iconColorS;
  final FocusNode? focusNode;
  final TextInputType? inputType;
  final String? hintText;
  final TextEditingController controller;
  final bool? isUpperCase;
  final double? width;
  final double? contentPadding;
  final InputBorder? border;
  final TextStyle? labelStyle;

  @override
  State<DateInput> createState() => _DateInputState();
}

/// RestorationProperty objects can be used because of RestorationMixin.
class _DateInputState extends State<DateInput> with RestorationMixin {
  // In this example, the restoration ID for the mixin is passed in through
  // the [StatefulWidget]'s constructor.
  @override
  String? get restorationId => widget.restorationId;
  final int year = DateTime.now().year;
  final int month = DateTime.now().month;
  final int day = DateTime.now().day;

  late final RestorableDateTime _selectedDate =
      RestorableDateTime(DateTime(year, month, day));

  late final RestorableRouteFuture<DateTime?> _restorableDatePickerRouteFuture =
      RestorableRouteFuture<DateTime?>(
    onComplete: _selectDate,
    onPresent: (NavigatorState navigator, Object? arguments) {
      return navigator.restorablePush(
        _datePickerRoute,
        arguments: _selectedDate.value.millisecondsSinceEpoch,
      );
    },
  );

  get iconColorS => widget.iconColorS ?? Colors.white;

  @pragma('vm:entry-point')
  static Route<DateTime> _datePickerRoute(
    BuildContext context,
    Object? arguments,
  ) {
    return DialogRoute<DateTime>(
      context: context,
      builder: (BuildContext context) {
        var year = DateTime.now().year;
        return DatePickerDialog(
          restorationId: 'date_picker_dialog',
          initialEntryMode: DatePickerEntryMode.calendarOnly,
          initialDate: DateTime.fromMillisecondsSinceEpoch(arguments! as int),
          firstDate: DateTime(year - 1),
          lastDate: DateTime(year + 1),
        );
      },
    );
  }

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    registerForRestoration(_selectedDate, 'selected_date');
    registerForRestoration(
        _restorableDatePickerRouteFuture, 'date_picker_route_future');
  }

  void _selectDate(DateTime? newSelectedDate) {
    if (newSelectedDate != null) {
      setState(() {
        _selectedDate.value = newSelectedDate;
        widget.controller.text =
            '${_selectedDate.value.day}/${_selectedDate.value.month}/${_selectedDate.value.year}';
      });
      FocusScope.of(context).unfocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    InputDecoration style = InputDecoration(
      contentPadding: (widget.contentPadding != null)
          ? EdgeInsets.all(widget.contentPadding!)
          : null,
      icon: (widget.icon != null) ? Icon(widget.icon!) : null,
      iconColor: Colors.blue,
      enabledBorder: widget.border ??
          const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black),
            borderRadius: BorderRadius.all(Radius.circular(1.0)),
            gapPadding: 2.0,
          ),
      border: widget.border ??
          const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black),
            borderRadius: BorderRadius.all(Radius.circular(1.0)),
            gapPadding: 1.0,
          ),
      hintText: (widget.hintText != null) ? widget.hintText : null,
      hintTextDirection: TextDirection.rtl,
      hintStyle:
          const TextStyle(fontStyle: FontStyle.italic, color: Colors.white),
      labelText: (widget.label != null) ? widget.label : null,
      labelStyle: widget.labelStyle ??
          const TextStyle(fontStyle: FontStyle.italic, color: Colors.white),
      alignLabelWithHint: true,
      suffixIcon: Icon(Icons.calendar_month, color: iconColorS),
    );

    // Date input field
    Widget dateInput = Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        style: const TextStyle(
            fontStyle: FontStyle.normal,
            color: Colors.black,
            fontWeight: FontWeight.w300),
        controller: widget.controller,
        onTap: () {
          _restorableDatePickerRouteFuture.present();
          FocusScope.of(context).unfocus();
        },
        keyboardType: TextInputType.datetime,
        // focusNode: widget.focusNode,
        decoration: style,
        // The validator receives the text that the user has entered.
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter ${(widget.label != null) ? widget.label!.toLowerCase() : 'date'}';
          }
          return null;
        },
      ),
    );

    // If width is not null, wrap the dateInput with SizedBox
    if (widget.width != null) {
      dateInput = SizedBox(
        width: widget.width,
        child: dateInput,
      );
    }

    return dateInput;
  }
}

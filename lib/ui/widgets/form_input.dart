import 'package:flutter/material.dart';

class InputForm extends StatelessWidget {
  final String? label;
  final IconData? icon;
  final FocusNode focusNode;
  final TextInputType? inputType;
  final String? hintText;
  final TextEditingController controller;
  final bool? isUpperCase;
  final double? width;
  final double? contentPadding;
  final TextStyle? labelStyle;
  final TextStyle? hintStyle;
  final InputBorder? border;
  final Widget? prefix;
  final int? maxLines;
  final TextDirection? hintTextDirection;
  final Color? iconColor;

  const InputForm(
      {super.key,
      this.label,
      this.icon,
      required this.focusNode,
      this.inputType,
      this.hintText,
      required this.controller,
      this.isUpperCase,
      this.width,
      this.contentPadding,
      this.labelStyle,
      this.hintStyle,
      this.border,
      this.prefix,
      this.maxLines,
      this.hintTextDirection,
      this.iconColor});

  @override
  Widget build(BuildContext context) {
    InputDecoration style = InputDecoration(
      icon: (icon != null) ? Icon(icon) : null,
      iconColor: iconColor ?? Colors.white,
      enabledBorder: border ??
          const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black),
            borderRadius: BorderRadius.all(Radius.circular(1.0)),
            gapPadding: 2.0,
          ),
      border: border ??
          const OutlineInputBorder(
            borderSide: BorderSide(color: Colors.black),
            borderRadius: BorderRadius.all(Radius.circular(1.0)),
            gapPadding: 1.0,
          ),
      hintText: (hintText != null) ? hintText : null,
      hintTextDirection: hintTextDirection ?? TextDirection.rtl,
      hintStyle: hintStyle ??
          const TextStyle(fontStyle: FontStyle.italic, color: Colors.white),
      labelText: (label != null) ? label : null,
      labelStyle: labelStyle ??
          const TextStyle(fontStyle: FontStyle.italic, color: Colors.white),
      floatingLabelStyle: labelStyle ??
          const TextStyle(fontStyle: FontStyle.italic, color: Colors.black),
      alignLabelWithHint: true,
      contentPadding:
          (contentPadding != null) ? EdgeInsets.all(contentPadding!) : null,
      prefixIcon: prefix,
    );

    Widget inputFormWidget = Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        style: const TextStyle(
            fontStyle: FontStyle.normal,
            color: Colors.black,
            fontWeight: FontWeight.w300),
        controller: controller,
        onTap: () => focusNode.requestFocus(),
        keyboardType: inputType ?? TextInputType.text,
        textCapitalization: (isUpperCase != null)
            ? TextCapitalization.none
            : TextCapitalization.words,
        focusNode: focusNode,
        decoration: style,
        maxLines: maxLines,
        // The validator receives the text that the user has entered.
        validator: (value) {
          if (value == null || value.isEmpty) {
            return 'Please enter ${(label != null) ? label!.toLowerCase() : 'value'}';
          }
          return null;
        },
      ),
    );

    if (width != null) {
      inputFormWidget = SizedBox(
        width: width,
        child: inputFormWidget,
      );
    }

    return inputFormWidget;
  }
}

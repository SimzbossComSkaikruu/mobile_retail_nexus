import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DrawerWidgetMenu extends StatelessWidget {
  final VoidCallback? onClicked;
  final Color? color;
  final Widget? icon;

  const DrawerWidgetMenu({super.key, this.onClicked, this.color, this.icon});

  @override
  Widget build(BuildContext context) => IconButton(
      onPressed: onClicked ?? () => Navigator.of(context).pop(true),
      color: (color != null) ? color : Colors.black87,
      icon: icon ?? const FaIcon(FontAwesomeIcons.alignLeft));
}

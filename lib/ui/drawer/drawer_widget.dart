import 'package:flutter/material.dart';
import 'drawer_item.dart';
import 'drawer_items.dart';

class DrawerWidget extends StatelessWidget {
  final ValueChanged<DrawerItem> onSelectedItem;

  const DrawerWidget({super.key, required this.onSelectedItem});

  get padding => const EdgeInsets.symmetric(horizontal: 24, vertical: 8);

  @override
  Widget build(BuildContext context) => Container(
        padding: const EdgeInsets.fromLTRB(16, 32, 16, 0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              _buildDrawerItems(context),
            ],
          ),
        ),
      );

  Widget _buildDrawerItems(BuildContext context) =>
      Column(children: _drawerItemsList());

  List<ListTile> _drawerItemsList() => DrawerItems.all
      .map(
        (items) => ListTile(
          onTap: () => onSelectedItem(items),
          contentPadding: padding,
          leading: Icon(items.icon, color: Colors.white),
          title: Text(
            items.title,
            style: const TextStyle(color: Colors.white),
          ),
        ),
      )
      .toList();
}

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:retail/ui/drawer/drawer_item.dart';

class DrawerItems {
  static const home = DrawerItem(title: 'Home', icon: FontAwesomeIcons.house);
  static const sales = DrawerItem(title: 'Sales', icon: FontAwesomeIcons.moneyBill);
  static const inventory = DrawerItem(title: 'Inventory', icon: FontAwesomeIcons.folderOpen);
  static const reports = DrawerItem(title: 'reports', icon: FontAwesomeIcons.folder);
  static const settings = DrawerItem(title: 'settings', icon: FontAwesomeIcons.toolbox);
  static const products = DrawerItem(title: 'products', icon: FontAwesomeIcons.cartPlus);
  static const logout = DrawerItem(title: 'logout', icon: FontAwesomeIcons.arrowRightFromBracket);

  static final List<DrawerItem> all = [
    home,
    sales,
    inventory,
    products,
    reports,
    settings,
    logout
  ];
}

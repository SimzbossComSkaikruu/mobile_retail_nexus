import 'package:retail/data/local/contracts/builder.dart';
import 'package:retail/data/local/schema/column.constants.dart';
import 'package:retail/data/local/schema/eloquent.builder.dart';
import 'package:retail/data/local/schema/relationship.builder.dart';
import '../db_conn/db.helpers.dart';
import '../db_conn/db.sqlite.conn.dart';

class StockMovementModel extends RelationshipModel {
  int? primaryKey;
  final String date;
  final String stockMovement;
  final String product;
  final int productId;
  final String currency;
  final String rate;
  final double quantity;
  final double buyingPrice;
  final double sellingPrice;
  final String description;
  final String? createdAt;
  final String? updatedAt;
  final String entryType;
  final double actualPriceCOGS;

  StockMovementModel({
    this.primaryKey,
    required this.stockMovement,
    required this.date,
    required this.product,
    required this.productId,
    required this.currency,
    required this.rate,
    required this.quantity,
    required this.buyingPrice,
    required this.sellingPrice,
    required this.description,
    this.createdAt,
    this.updatedAt,
    required this.entryType,
    required this.actualPriceCOGS,
  })  : assert(quantity >= 0),
        assert(buyingPrice >= 0),
        assert(sellingPrice >= 0);

  // createdAt = DateTime.now().toString(),
  // updatedAt = DateTime.now().toString();

  @override
  Eloquent get eloquent => StockMovementEloquent();

  @override
  get primaryValue => primaryKey.toString();

  @override
  setPrimaryValue(value) => primaryKey = value;

  @override
  Map<String, Object?> toJson() => {
        'stockMovement': stockMovement,
        'date': date,
        'product': product,
        'productId': productId,
        'currency': currency,
        'rate': rate,
        'quantity': quantity,
        'buyingPrice': buyingPrice,
        'sellingPrice': sellingPrice,
        'description': description,
        'entryType': entryType,
        'actualPriceCOGS': actualPriceCOGS,
      };

  StockMovementModel copy({
    int? primaryKey,
    String? date,
    String? stockMovement,
    String? product,
    int? productId,
    String? currency,
    String? rate,
    double? quantity,
    double? buyingPrice,
    double? sellingPrice,
    String? description,
    String? createdAt,
    String? updatedAt,
    String? entryType,
    double? actualPriceCOGS,
  }) =>
      StockMovementModel(
        primaryKey: primaryKey ?? this.primaryKey,
        date: date ?? this.date,
        stockMovement: stockMovement ?? this.stockMovement,
        product: product ?? this.product,
        productId: productId ?? this.productId,
        currency: currency ?? this.currency,
        rate: rate ?? this.rate,
        quantity: quantity ?? this.quantity,
        buyingPrice: buyingPrice ?? this.buyingPrice,
        sellingPrice: sellingPrice ?? this.sellingPrice,
        description: description ?? this.description,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        entryType: entryType ?? this.entryType,
        actualPriceCOGS: actualPriceCOGS ?? this.actualPriceCOGS,
      );

  static StockMovementModel fromJson(Map<String, Object?> first) =>
      StockMovementModel(
        primaryKey: first['primaryKey'] as int?,
        stockMovement: first['stockMovement'] as String,
        date: first['date'] as String,
        product: first['product'] as String,
        productId: first['productId'] as int,
        currency: first['currency'] as String,
        rate: first['rate'] as String,
        quantity: first['quantity'] as double,
        buyingPrice: first['buyingPrice'] as double,
        sellingPrice: first['sellingPrice'] as double,
        description: first['description'] as String,
        createdAt: first['createdAt'] as String,
        updatedAt: first['updatedAt'] as String,
        entryType: first['entryType'] as String,
        actualPriceCOGS: first['actualPriceCOGS'] as double,
      );

  @override
  String toString() {
    return 'StockMovement{primaryKey: $primaryKey, date: $date, stockMovement: $stockMovement, product: $product, productId: $productId, currency: $currency, rate: $rate, quantity: $quantity, buyingPrice: $buyingPrice, sellingPrice: $sellingPrice, description: $description, createdAt: $createdAt, updatedAt: $updatedAt, entryType: $entryType, actualPriceCOGS: $actualPriceCOGS}';
  }

  @override
  Future<void> createTable() {
    throw UnimplementedError();
  }
}

class StockMovementEloquent extends Eloquent {
  @override
  String get tableName => 'stock_movements';

  @override
  String get getPrimaryColumn => 'primaryKey';

  @override
  List<String> get columns => [
        'primaryKey',
        'stockMovement',
        'date',
        'product',
        'productId',
        'currency',
        'rate',
        'quantity',
        'buyingPrice',
        'sellingPrice',
        'description',
        'createdAt',
        'updatedAt',
        'actualPriceCOGS',
        'entryType'
      ];

  @override
  Future<void> createTable() async {
    final db = await SqliteDatabaseProvider.instance.getDatabase();
    return Helpers.createTable(db, tableName: tableName, columns: {
      'primaryKey': Constants.idType,
      'stockMovement': Constants.textType,
      'date': Constants.textType,
      'product': Constants.textType,
      'productId': Constants.integerType,
      'currency': Constants.textType,
      'rate': Constants.textType,
      'quantity': Constants.realType,
      'buyingPrice': Constants.realType,
      'sellingPrice': Constants.realType,
      'description': Constants.textType,
      'createdAt': Constants.timestamp,
      'updatedAt': Constants.timestamp,
      'entryType': Constants.textNullType,
      'actualPriceCOGS': Constants.realType
    });
  }
}

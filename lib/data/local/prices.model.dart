import 'package:retail/data/local/schema/column.constants.dart';
import 'package:retail/data/local/schema/eloquent.builder.dart';
import 'package:retail/data/local/schema/relationship.builder.dart';
import 'package:retail/data/local/schema/relationships/many_to_many.dart';
import 'package:retail/data/local/schema/relationships/one_to_many.dart';

import '../db_conn/db.helpers.dart';
import '../db_conn/db.sqlite.conn.dart';


class PricesModel extends RelationshipModel with OneToMany, ManyToMany {
  int? primaryKey;
  final String productName;
  final String productCode;
  final int productId;
  final double price;
  final String? createdAt;
  final String? updatedAt;
  final String currency;
  final double rate;

  PricesModel({
    this.primaryKey,
    required this.productName,
    required this.productCode,
    required this.productId,
    required this.price,
    this.createdAt,
    this.updatedAt,
    required this.currency,
    required this.rate,
  });

  @override
  Map<String, Object?> toJson() => {
        'productName': productName,
        'productCode': productCode,
        'productId': productId,
        'price': price,
        'currency': currency,
        'rate': rate,
      };

  PricesModel copy({
    int? primaryKey,
    String? productName,
    String? productCode,
    int? productId,
    double? price,
    String? createdAt,
    String? updatedAt,
    String? currency,
    double? rate,
  }) =>
      PricesModel(
        primaryKey: primaryKey ?? this.primaryKey,
        productName: productName ?? this.productName,
        productCode: productCode ?? this.productCode,
        productId: productId ?? this.productId,
        price: price ?? this.price,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        currency: currency ?? this.currency,
        rate: rate ?? this.rate,
      );

  static PricesModel fromJson(Map<String, Object?> first) => PricesModel(
        primaryKey: first['primaryKey'] as int?,
        productName: first['productName'] as String,
        productCode: first['productCode'] as String,
        productId: first['productId'] as int,
        price: first['price'] as double,
        createdAt: first['createdAt'] as String?,
        updatedAt: first['updatedAt'] as String?,
        currency: first['currency'] as String,
        rate: first['rate'] as double,
      );

  @override
  Future<void> createTable() {
    throw UnimplementedError();
  }

  @override
  Eloquent get eloquent => PriceEloquent();

  @override
  get primaryValue => primaryKey.toString();

  @override
  setPrimaryValue(value) => primaryKey = value;
}

class PriceEloquent extends Eloquent {

  @override
  List<String> get columns => [
        'primaryKey',
        'productName',
        'productCode',
        'productId',
        'price',
        'createdAt',
        'updatedAt',
        'currency',
        'rate',
      ];

  @override
  Future<void> createTable() async {
    final db = await SqliteDatabaseProvider.instance.getDatabase();
    return Helpers.createTable(db, tableName: tableName, columns: {
      'primaryKey': Constants.idType,
      'productName': Constants.textType,
      'productCode': Constants.textType,
      'productId': Constants.integerType,
      'price': Constants.realType,
      'createdAt': Constants.timestamp,
      'updatedAt': Constants.timestamp,
      'currency': Constants.textType,
      'rate': Constants.realType,
    });
  }

  @override
  String get getPrimaryColumn => 'primaryKey';

  @override
  String get tableName => 'prices';
}

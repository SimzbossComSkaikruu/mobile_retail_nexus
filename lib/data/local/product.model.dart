import 'package:retail/data/local/schema/column.constants.dart';
import 'package:retail/data/local/schema/eloquent.builder.dart';
import 'package:retail/data/local/schema/relationship.builder.dart';
import 'package:retail/data/local/schema/relationships/many_to_many.dart';
import 'package:retail/data/local/schema/relationships/one_to_many.dart';
import '../db_conn/db.helpers.dart';
import '../db_conn/db.sqlite.conn.dart';

const String isStockedDefault = 'true';

class ProductModel extends RelationshipModel with OneToMany, ManyToMany {
  int? primaryKey;
  final String productName;
  final String productCode;
  final String productCategory;
  final String measurementUnit;
  final String description;
  final String price;
  final String purchasePrice;
  final String quantity;
  final String minimumQuantity;
  final String openingDate;
  final String? isStocked;
  final String? createdAt;
  final String? updatedAt;
  final String currency;
  final double rate;

  ProductModel({
    this.primaryKey,
    required this.productName,
    required this.productCode,
    required this.productCategory,
    required this.measurementUnit,
    required this.description,
    required this.price,
    required this.purchasePrice,
    required this.quantity,
    required this.minimumQuantity,
    required this.openingDate,
    this.isStocked,
    this.createdAt,
    this.updatedAt,
    required this.currency,
    required this.rate,
  });

  @override
  Map<String, Object?> toJson() => {
        'productName': productName,
        'productCode': productCode,
        'productCategory': productCategory,
        'measurementUnit': measurementUnit,
        'description': description,
        'price': price,
        'purchasePrice': purchasePrice,
        'quantity': quantity,
        'minimumQuantity': minimumQuantity,
        'openingDate': openingDate,
        'isStocked': isStocked,
        'currency': currency,
        'rate': rate,
      };

  ProductModel copy({
    int? primaryKey,
    String? productName,
    String? productCode,
    String? productCategory,
    String? measurementUnit,
    String? description,
    String? price,
    String? purchasePrice,
    String? quantity,
    String? minimumQuantity,
    String? openingDate,
    String? isStocked,
    String? createdAt,
    String? updatedAt,
    String? currency,
    double? rate,
  }) =>
      ProductModel(
        primaryKey: primaryKey! ?? this.primaryKey,
        productName: productName ?? this.productName,
        productCode: productCode ?? this.productCode,
        productCategory: productCategory ?? this.productCategory,
        measurementUnit: measurementUnit ?? this.measurementUnit,
        description: description ?? this.description,
        price: price ?? this.price,
        purchasePrice: purchasePrice ?? this.purchasePrice,
        quantity: quantity ?? this.quantity,
        minimumQuantity: minimumQuantity ?? this.minimumQuantity,
        openingDate: openingDate ?? this.openingDate,
        isStocked: isStocked ?? this.isStocked,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        currency: currency ?? this.currency,
        rate: rate ?? this.rate,
      );

  static ProductModel fromJson(Map<String, Object?> first) {
    return ProductModel(
      primaryKey: first['primaryKey'] as int?,
      productName: first['productName'] as String,
      productCode: first['productCode'] as String,
      productCategory: first['productCategory'] as String,
      measurementUnit: first['measurementUnit'] as String,
      description: first['description'] as String,
      price: first['price'] as String,
      purchasePrice: first['purchasePrice'] as String,
      quantity: first['quantity'] as String,
      minimumQuantity: first['minimumQuantity'] as String,
      openingDate: first['openingDate'] as String,
      isStocked: first['isStocked'] as String?,
      createdAt: first['createdAt'] as String,
      updatedAt: first['updatedAt'] as String,
      currency: first['currency'] as String,
      rate: first['rate'] as double,
    );
  }

  @override
  String toString() {
    return 'Product{ primaryKey: $primaryKey, productName: $productName, productCode: $productCode, productCategory: $productCategory, measurementUnit: $measurementUnit, description: $description, price: $price, purchasePrice: $purchasePrice, quantity: $quantity, minimumQuantity: $minimumQuantity, openingDate: $openingDate, isStocked: $isStocked, createdAt: $createdAt, updatedAt: $updatedAt}';
  }

  @override
  Eloquent get eloquent => ProductEloquent();

  @override
  get primaryValue => primaryKey.toString();

  @override
  setPrimaryValue(value) => primaryKey = value;

  @override
  Future<void> createTable() {
    throw UnimplementedError();
  }

  //one product hasMany stock movements
  Future<RelationshipModel> getStockMovements() async {
    return await hasMany('stock_movements', foreignKey: 'productId', parentKey: 'primaryKey');
  }
}

class ProductEloquent extends Eloquent {
  @override
  List<String> get columns => [
        'primaryKey',
        'productName',
        'productCode',
        'productCategory',
        'measurementUnit',
        'description',
        'price',
        'purchasePrice',
        'quantity',
        'minimumQuantity',
        'openingDate',
        'isStocked',
        'createdAt',
        'updatedAt',
        'currency',
        'rate',
      ];

  @override
  Future<void> createTable() async {
    final db = await SqliteDatabaseProvider.instance.getDatabase();
    return Helpers.createTable(db, tableName: tableName, columns: {
      'primaryKey': Constants.idType,
      'productName': Constants.textType,
      'productCode': Constants.textType,
      'productCategory': Constants.textType,
      'measurementUnit': Constants.textType,
      'description': Constants.textType,
      'price': Constants.textType,
      'purchasePrice': Constants.textType,
      'quantity': Constants.textType,
      'minimumQuantity': Constants.textType,
      'openingDate': Constants.textType,
      'isStocked': '${Constants.textType}  ${Constants.defaultValue(isStockedDefault)}',
      'createdAt': Constants.timestamp,
      'updatedAt': Constants.timestamp,
      'currency': Constants.textType,
      'rate': Constants.doubleType,
    });
  }

  @override
  String get getPrimaryColumn => 'primaryKey';

  @override
  String get tableName => 'products';
}

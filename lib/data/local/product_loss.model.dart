import 'package:retail/data/local/schema/column.constants.dart';
import 'package:retail/data/local/schema/eloquent.builder.dart';
import 'package:retail/data/local/schema/relationship.builder.dart';
import 'package:retail/data/local/schema/relationships/many_to_many.dart';
import 'package:retail/data/local/schema/relationships/one_to_many.dart';

import '../db_conn/db.helpers.dart';
import '../db_conn/db.sqlite.conn.dart';

class ProductLossModel extends RelationshipModel with OneToMany, ManyToMany {
  int? primaryKey;
  final int productId;
  final double quantity;
  final double averageCostOfProduct;
  final double totalCost;
  final String reasonForLoss;
  final String description;
  final String dateCapture;
  final String product;
  final String? createdAt;
  final String? updatedAt;

  ProductLossModel({
    this.primaryKey,
    required this.productId,
    required this.quantity,
    required this.averageCostOfProduct,
    required this.totalCost,
    required this.reasonForLoss,
    required this.description,
    required this.dateCapture,
    required this.product,
    this.createdAt,
    this.updatedAt,
  });

  @override
  Future<void> createTable() {
    throw UnimplementedError();
  }

  @override
  Eloquent get eloquent => ProductLossEloquent();

  @override
  get primaryValue => primaryKey.toString();

  @override
  setPrimaryValue(value) => primaryKey = value;

  @override
  Map<String, Object?> toJson() {
    return {
      'productId': productId,
      'quantity': quantity,
      'averageCostOfProduct': averageCostOfProduct,
      'totalCost': totalCost,
      'reasonForLoss': reasonForLoss,
      'description': description,
      'dateCapture': dateCapture,
      'product': product,
    };
  }
}

class ProductLossEloquent extends Eloquent {
  @override
  String get tableName => 'product_loss';

  @override
  Future<void> createTable() async {
    final db = await SqliteDatabaseProvider.instance.getDatabase();
    return Helpers.createTable(db, tableName: tableName, columns: {
      'primaryKey': Constants.idType,
      'productId': Constants.intType,
      'quantity': Constants.doubleType,
      'averageCostOfProduct': Constants.doubleType,
      'totalCost': Constants.doubleType,
      'reasonForLoss': Constants.textType,
      'description': Constants.textType,
      'dateCapture': Constants.textType,
      'product': Constants.textType,
      'createdAt': Constants.timestamp,
      'updatedAt': Constants.timestamp,
    });
  }

  @override
  String get getPrimaryColumn => 'primaryKey';

  @override
  List<String> get columns => [
        'primaryKey',
        'productId',
        'quantity',
        'averageCostOfProduct',
        'totalCost',
        'reasonForLoss',
        'description',
        'dateCapture',
        'product',
        'createdAt',
        'updatedAt',
      ];
}

import 'package:retail/data/local/schema/column.constants.dart';
import 'package:retail/data/local/schema/eloquent.builder.dart';
import 'package:retail/data/local/schema/relationship.builder.dart';
import 'package:retail/data/local/schema/relationships/many_to_many.dart';
import 'package:retail/data/local/schema/relationships/one_to_many.dart';

import '../db_conn/db.helpers.dart';
import '../db_conn/db.sqlite.conn.dart';

class Receipt extends RelationshipModel with OneToMany, ManyToMany {
  int? primaryKey;
  final String currencyName;
  final String currencyCode;
  final double rate;
  final String? createdAt;
  final String? updatedAt;
  final String customerName;
  final String customerPhoneNumber;
  final String totalAmount;
  final String status;

  Receipt({
    this.primaryKey,
    required this.currencyName,
    required this.currencyCode,
    required this.rate,
    required this.customerName,
    required this.customerPhoneNumber,
    required this.totalAmount,
    required this.status,
    this.createdAt,
    this.updatedAt,
  });

  @override
  Future<void> createTable() {
    throw UnimplementedError();
  }

  @override
  Eloquent get eloquent => ReceiptEloquent();

  @override
  get primaryValue => primaryKey;

  @override
  setPrimaryValue(value) => primaryKey = value;

  @override
  Map<String, Object?> toJson() {
    return {
      'currencyName': currencyName,
      'currencyCode': currencyCode,
      'rate': rate,
      'customerName': customerName,
      'customerPhoneNumber': customerPhoneNumber,
      'totalAmount': totalAmount,
      'status': status,
    };
  }

  Receipt copy({
    int? primaryKey,
    String? currencyName,
    String? currencyCode,
    double? rate,
    String? createdAt,
    String? updatedAt,
    String? customerName,
    String? customerPhoneNumber,
    String? totalAmount,
    String? status,
  }) =>
      Receipt(
        primaryKey: primaryKey ?? this.primaryKey,
        currencyName: currencyName ?? this.currencyName,
        currencyCode: currencyCode ?? this.currencyCode,
        rate: rate ?? this.rate,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
        customerName: customerName ?? this.customerName,
        customerPhoneNumber: customerPhoneNumber ?? this.customerPhoneNumber,
        totalAmount: totalAmount ?? this.totalAmount,
        status: status ?? this.status,
      );

  static Receipt fromJson(Map<String, Object?> first) => Receipt(
        primaryKey: first['primaryKey'] as int?,
        currencyName: first['currencyName'] as String,
        currencyCode: first['currencyCode'] as String,
        rate: first['rate'] as double,
        createdAt: first['createdAt'] as String?,
        updatedAt: first['updatedAt'] as String?,
        customerName: first['customerName'] as String,
        customerPhoneNumber: first['customerPhoneNumber'] as String,
        totalAmount: first['totalAmount'] as String,
        status: first['status'] as String,
      );
}

class ReceiptEloquent extends Eloquent {
  @override
  String get getPrimaryColumn => 'primaryKey';

  @override
  String get tableName => 'receipts';

  @override
  List<String> get columns => [
        'primaryKey',
        'currencyName',
        'currencyCode',
        'rate',
        'createdAt',
        'updatedAt',
        'customerName',
        'customerPhoneNumber',
        'totalAmount',
        'status',
      ];

  @override
  Future<void> createTable() async {
    final db = await SqliteDatabaseProvider.instance.getDatabase();
    return Helpers.createTable(db, tableName: tableName, columns: {
      'primaryKey': Constants.idType,
      'currencyName': Constants.textType,
      'currencyCode': Constants.textType,
      'rate': Constants.realType,
      'customerName': Constants.textType,
      'customerPhoneNumber': Constants.textType,
      'totalAmount': Constants.textType,
      'status': Constants.textType,
      'createdAt': Constants.timestamp,
      'updatedAt': Constants.timestamp,
    });
  }
}

class Constants {
  static const String idType = 'INTEGER PRIMARY KEY AUTOINCREMENT';
  static const String textType = 'TEXT NOT NULL';
  static const String intType = 'INTEGER NOT NULL';
  static const String textNullType = 'TEXT';
  static const String notNull = 'NOT NULL';
  static defaultValue(value) => 'DEFAULT "$value"';
  static const String primaryKey = 'PRIMARY KEY';
  static const String boolType = 'BOOLEAN';
  static const String integerType = 'INTEGER';
  static const String stringType = 'TEXT';
  static const String realType = 'REAL';
  static const String unique = 'UNIQUE';
  static const String timestamp = 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP';
  static const String doubleType = 'REAL';

  static String enumToString(List<String> types) {
    if (types.isEmpty) {
      return '';
    }
    var string = '';
    for (var type in types) {
      string += ' $type';
    }
    return string;
  }
}

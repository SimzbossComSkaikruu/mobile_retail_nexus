import 'package:sqflite/sqflite.dart';
import '../../db_conn/db.sqlite.conn.dart';
import 'generator.builder.dart';

abstract class Eloquent with Generator {
  Future<Database> get database => SqliteDatabaseProvider.instance.getDatabase();

  //get the columns names of the table
  Future<List<String>> getColumnNames({String? table}) async {
    Database db = await database;
    var data = await db.rawQuery("PRAGMA table_info(${table ?? tableName})", null);
    return data.map((e) => e['name'].toString()).toList();
  }

  //get the foreign keys in the table
  Future<List<Map<String, dynamic>>> getForeignKeys({String? table}) async {
    Database db = await database;
    var data = await db.rawQuery("PRAGMA foreign_key_list(${table ?? tableName})", null);
    return data;
  }

  @override
  Future<List<Map<String, Object?>>> all() async {
    String query = 'SELECT ';
    try {
      Database db = await database;
      resetAll();
      query += generateQuery('* from $tableName');
      return await db.rawQuery(query);
    } catch (e) {
      throw Exception('Generated query: "$query" \n$e');
    }
  }

  /// Final execution of query is performed by issuing this method.
  /// ```
  /// var userEloquent = UserEloquent();
  /// userEloquent.get();
  /// ```
  @override
  Future<List<Map<String, Object?>>> get() async {
    String q = 'Select';
    try {
      String selectedColumns = getSelectedColumns() ?? '*';
      q += generateQuery(' $selectedColumns from $tableName');

      resetAll();

      Database db = await database;
      return await db.rawQuery(q);
    } catch (e) {
      throw Exception('Generated query: "$q" \n$e');
    }
  }

  /// Find row by primary key.
  ///
  /// ```dart
  /// var userEloquent = UserEloquent();
  ///
  /// // get user where primary key (id) is 1.
  /// userEloquent.find(1);
  /// ```
  @override
  Future<Map<String, Object?>?> find(primaryKeyValue) async {
    Database db = await database;
    var results = await db.query(
      tableName,
      columns: columns,
      where: '$getPrimaryColumn = ?',
      whereArgs: [primaryKeyValue],
    );
    resetAll();
    if (results.isNotEmpty) {
      return results[0];
    }
    return null;
  }

  /// Search rows.
  ///
  /// ```dart
  /// var userEloquent = UserEloquent();
  ///
  /// // get rows where any column has word 'j'.
  /// userEloquent.search('j');
  ///
  /// // get rows where country has 'UK' and any other rows has 'j'.
  /// userEloquent.where('country','UK').search('j');
  ///
  /// //specify searchable columns
  /// userEloquent.search('j',searchableColumns:['name']);
  /// ```
  @override
  Future<List<Map<String, Object?>>> search(String keyword, {List<String>? searchableColumns}) async {
    String key = '%$keyword%';
    String q = 'Select';
    try {
      List<String>? usedColumns;
      var wheres = getWhereColumns();
      if (wheres.isNotEmpty) {
        usedColumns = wheres.map((e) => e.columnName).toList();
      }
      if (searchableColumns != null && searchableColumns.isNotEmpty) {
        for (var column in searchableColumns) {
          where(column, key, operator: Operator.like, conjuncation: 'or');
        }
      } else {
        for (var column in columns) {
          if (usedColumns != null && usedColumns.contains(column)) {
            continue;
          }
          where(column, key, operator: Operator.like, conjuncation: 'or');
        }
      }
      q += generateQuery(getSelectedColumns() ?? ' * from $tableName');
      resetAll();
      Database db = await database;
      return await db.rawQuery(q);
    } catch (e) {
      throw Exception('Generated query: "$q" \n$e');
    }
  }

  /// Create a new row.
  /// ```dart
  /// var userEloquent = UserEloquent();
  ///
  /// userEloquent.create({'name':'John','password':'pass'});
  ///
  /// ```
  @override
  Future<int> create(Map<String, Object?> values) async {
    resetAll();
    final db = await database;
    return await db.insert(
      tableName,
      values,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Map<String, Object?>>> _where(Map<String, Object?> object) async {
    Database db = await database;
    var where = '';
    var whereArgs = [];
    object.forEach((key, value) {
      where = where == '' ? '$key = ?' : '$where and $key = ?';
      whereArgs.add(value);
    });
    return await db.query(tableName, columns: columns, where: where, whereArgs: whereArgs);
  }

  /// Create a new row only if the value is not existed.
  ///
  /// ```dart
  /// var userEloquent = UserEloquent();
  ///
  // create user which name is john and password is pass only if name 'john' is not existed.
  /// userEloquent.createIfNotExists(check:{'name':'john'},create:{'password':'pass'});
  ///
  /// ```
  @override
  Future<int?> createIfNotExists({required Map<String, Object?> check, required Map<String, Object?> create}) async {
    final db = await database;
    List result = await _where(check);
    if (result.isNotEmpty) {
      return null;
    }
    create.addAll(check);
    resetAll();
    return await db.insert(tableName, create);
  }

  /// Update data if exists and if not, create new row.
  ///
  /// ```dart
  /// var userEloquent = UserEloquent();
  ///
  /// // if row where name is john exists, update 'password' column. If not, create row where name is john and password is 'pass'.
  /// userEloquent.updateOrCreate(check:{'name':'john'},inserts:{'password':'pass'});
  ///```
  @override
  Future<int> updateOrCreate({required Map<String, Object?> check, required Map<String, Object?> inserts}) async {
    final db = await database;
    List checkResult = await _where(check);
    if (checkResult.isNotEmpty) {
      var where = '';
      var whereArgs = [];
      check.forEach((key, value) {
        where = where == '' ? '$key = ?' : '$where and $key = ?';
        whereArgs.add(value);
      });
      await db.update(tableName, Map.fromEntries(inserts.entries.where((element) => element.key != 'isFavourite')),
          where: where, whereArgs: whereArgs);
      return 0;
    }
    resetAll();
    return await db.insert(tableName, {...check, ...inserts});
  }

  /// Update rows and return number of changes.
  /// ```dart
  /// var userEloquent = UserEloquent();
  ///
  /// // update name of all rows to 'john'.
  /// userEloquent.update({'name':'john'});
  ///
  /// // update name of rows where id = 1 to 1.
  /// userEloquent.where('id',1).update({'name':'john'});
  ///
  /// ```
  @override
  Future<int> update(Map<String, Object?> values) async {
    String q = 'Update $tableName';
    try {
      for (var val in values.entries) {
        if (columns.contains(val.key)) {
          q += ' SET ${val.key} = "${val.value}"';
          if (val.key != values.keys.last) {
            q += ',';
          }
        }
      }
      resetDistinct();
      resetGroupBy();
      resetSelectedColumns();
      resetSort();
      var selectQuery = generateQuery('Select $getPrimaryColumn from $tableName');
      q += ' WHERE $tableName.$getPrimaryColumn IN ($selectQuery)';
      resetAll();
      final db = await database;
      return await db.rawUpdate(q);
    } catch (e) {
      throw Exception('Generated query: "$q" \n$e');
    }
  }

  ///   Delete rows from table and return number of changes.
  ///
  /// ```dart
  /// var userEloquent = UserEloquent();
  ///
  /// // delete all rows from users
  /// userEloquent.delete();
  ///
  /// // delete rows where name has 'j' from users
  /// userEloquent.where('name','%j%',operator:Operator.like).delete();
  ///
  /// ```
  @override
  Future<int> delete() async {
    String query = 'Delete';
    try {
      resetSelectedColumns();
      resetDistinct();
      resetOrderBy();
      resetGroupBy();
      resetOffset();
      var selectQuery = generateQuery('Select $getPrimaryColumn from $tableName');
      query += ' FROM $tableName WHERE $tableName.$getPrimaryColumn IN ($selectQuery)';
      resetAll();

      Database db = await database;
      return await db.rawDelete(query);
    } catch (e) {
      throw Exception('Generated query: "$query" \n$e');
    }
  }

  ///  Delete a row by primary key.
  ///
  /// ```dart
  /// var userEloquent = UserEloquent();
  ///
  /// // delete row where primary key is 1
  /// userEloquent.deleteBy(1);
  /// ```
  Future<int> deleteBy(value) async {
    Database db = await database;
    return await db.delete(
      tableName,
      where: '$getPrimaryColumn = ?',
      whereArgs: [value],
    );
  }

  /// Update a record given its ID.
  ///
  /// ```dart
  /// var userEloquent = UserEloquent();
  ///
  /// // update the record with id 1, set name to 'John'
  /// userEloquent.updateById(1, {'name': 'John'});
  /// ```
  Future<int> updateById(int id, Map<String, Object?> values) async {
    Database db = await database;
    return await db.update(
      tableName,
      values,
      where: '$getPrimaryColumn = ?',
      whereArgs: [id],
    );
  }
}

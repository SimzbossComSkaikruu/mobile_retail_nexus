import 'package:retail/data/local/schema/column.constants.dart';
import 'package:retail/data/local/schema/eloquent.builder.dart';
import 'package:retail/data/local/schema/relationship.builder.dart';

import '../db_conn/db.helpers.dart';
import '../db_conn/db.sqlite.conn.dart';

class ReceiptLineModel extends RelationshipModel {
  int? primaryKey;
  final String productName;
  final String productCode;
  final int productId;
  final int receiptId;
  final String currency;
  final String currencyCode;
  final double rate;
  final double price;
  final double quantity;
  final double totalAmount;
  final double margin;
  final double profit;
  final String? createdAt;
  final String? updatedAt;

  ReceiptLineModel({
    this.primaryKey,
    required this.receiptId,
    required this.productName,
    required this.productCode,
    required this.productId,
    required this.currency,
    required this.currencyCode,
    required this.rate,
    required this.price,
    required this.quantity,
    required this.totalAmount,
    required this.margin,
    required this.profit,
    this.createdAt,
    this.updatedAt,
  });

  @override
  Map<String, Object?> toJson() => {
        'productName': productName,
        'productCode': productCode,
        'productId': productId,
        'receiptId': receiptId,
        'currency': currency,
        'currencyCode': currencyCode,
        'rate': rate,
        'price': price,
        'quantity': quantity,
        'totalAmount': totalAmount,
        'margin': margin,
        'profit': profit,
      };

  ReceiptLineModel copy({
    int? primaryKey,
    String? productName,
    String? productCode,
    int? productId,
    int? receiptId,
    String? currency,
    String? currencyCode,
    double? rate,
    double? price,
    double? quantity,
    double? totalAmount,
    double? margin,
    double? profit,
    String? createdAt,
    String? updatedAt,
  }) =>
      ReceiptLineModel(
        primaryKey: primaryKey ?? this.primaryKey,
        productName: productName ?? this.productName,
        productCode: productCode ?? this.productCode,
        productId: productId ?? this.productId,
        receiptId: receiptId ?? this.receiptId,
        currency: currency ?? this.currency,
        currencyCode: currencyCode ?? this.currencyCode,
        rate: rate ?? this.rate,
        price: price ?? this.price,
        quantity: quantity ?? this.quantity,
        totalAmount: totalAmount ?? this.totalAmount,
        margin: margin ?? this.margin,
        profit: profit ?? this.profit,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );

  static ReceiptLineModel fromJson(Map<String, Object?> first) => ReceiptLineModel(
        primaryKey: first['primaryKey'] as int?,
        productName: first['productName'] as String,
        productCode: first['productCode'] as String,
        productId: first['productId'] as int,
        receiptId: first['receiptId'] as int,
        currency: first['currency'] as String,
        currencyCode: first['currencyCode'] as String,
        rate: first['rate'] as double,
        price: first['price'] as double,
        quantity: first['quantity'] as double,
        totalAmount: first['totalAmount'] as double,
        margin: first['margin'] as double,
        profit: first['profit'] as double,
        createdAt: first['createdAt'] as String?,
        updatedAt: first['updatedAt'] as String?,
      );

  @override
  String get getPrimaryColumn => 'primaryKey';

  @override
  String toString() =>
      'ReceiptLineModel(primaryKey: $primaryKey, productName: $productName, productCode: $productCode, productId: $productId, currency: $currency, currencyCode: $currencyCode, rate: $rate, price: $price, quantity: $quantity, totalAmount: $totalAmount, margin: $margin, profit: $profit, createdAt: $createdAt, updatedAt: $updatedAt)';

  @override
  Future<void> createTable() {
    throw UnimplementedError();
  }

  @override
  get primaryValue => primaryKey.toString();

  @override
  setPrimaryValue(value) => primaryKey = value;

  @override
  Eloquent get eloquent => ReceiptLineEloquent();
}

class ReceiptLineEloquent extends Eloquent {
  @override
  String get tableName => 'receipt_lines';

  @override
  List<String> get columns => [
        'primaryKey',
        'productName',
        'productCode',
        'productId',
        'receiptId',
        'currency',
        'currencyCode',
        'rate',
        'price',
        'quantity',
        'totalAmount',
        'margin',
        'profit',
        'createdAt',
        'updatedAt',
      ];

  @override
  Future<void> createTable() async {
    final db = await SqliteDatabaseProvider.instance.getDatabase();
    return Helpers.createTable(db, tableName: tableName, columns: {
      'primaryKey': Constants.idType,
      'productName': Constants.textType,
      'productCode': Constants.textType,
      'productId': Constants.integerType,
      'receiptId': Constants.integerType,
      'currency': Constants.textType,
      'currencyCode': Constants.textType,
      'rate': Constants.realType,
      'price': Constants.realType,
      'quantity': Constants.realType,
      'totalAmount': Constants.realType,
      'margin': Constants.realType,
      'profit': Constants.realType,
      'createdAt': Constants.timestamp,
      'updatedAt': Constants.timestamp,
    });
  }

  @override
  String get getPrimaryColumn => 'primaryKey';
}

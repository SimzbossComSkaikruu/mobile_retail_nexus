import 'package:sqflite/sqflite.dart';

class Helpers {
  ///create table if not exists
  /// Create table using helper method
  static Future createTable(Database db,
      {required String tableName,
      required Map<String, Object?> columns}) async {
    var string = '';
    columns.forEach((key, value) {
      if (value is Function) {
        string += value();
      } else if (value is List<String>) {
        string += key;
        for (var type in value) {
          string += ' $type';
        }
      } else {
        string += '$key $value';
      }
      if (key != columns.entries.last.key) {
        string += ',';
      }
    });
    await db.execute('CREATE TABLE IF NOT EXISTS $tableName ($string)');
  }
}

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'db.config.dart';

class SqliteDatabaseProvider {
  static final SqliteDatabaseProvider instance = SqliteDatabaseProvider._init();
  static Database? _database;

  SqliteDatabaseProvider._init();

  Future<Database> getDatabase() async {
    if (_database != null) return _database!;
    _database = await _initDB(RetailDatabaseConfig.databaseName);
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);
    return await openDatabase(path,
        version: RetailDatabaseConfig.databaseVersion, onCreate: _createDB);
  }

  Future<void> _createDB(Database db, int version) async {
    //create a migration table
    db.execute('''
      CREATE TABLE migrations (id INTEGER PRIMARY KEY, migration TEXT NOT NULL, createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP)''');
  }

  ///delete database
  static Future<void> clear() async {
    await deleteDatabase(
        join(await getDatabasesPath(), RetailDatabaseConfig.databaseName));
  }
}

import 'package:flutter/material.dart';
import 'package:retail/ui/components/configuration/constants.dart';
import 'package:retail/ui/drawer/drawer_item.dart';
import 'package:retail/ui/drawer/drawer_items.dart';
import 'package:retail/ui/drawer/drawer_widget.dart';
import 'package:retail/ui/screens/home_page.dart';
import 'package:retail/ui/screens/inventory/index.dashboard.dart';
import 'package:retail/ui/screens/pos/index.pos.dart';
import 'package:retail/ui/screens/pos/point_of_sale.index.dart';
import 'package:retail/ui/screens/pos/ui_components/settings.dart';
import 'package:retail/ui/screens/product_service.dart';
import 'package:retail/ui/screens/report.dart';
import 'controllers/cart.dart';
import 'controllers/inventory_loss.controller.dart';
import 'controllers/product_controller.dart';
import 'controllers/receipt.sales.dart';
import 'controllers/report.chart.dart';
import 'controllers/stock_movement_controller.dart';
import 'data/db_conn/db.sqlite.conn.dart';
import 'package:provider/provider.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  //initialize the database
  SqliteDatabaseProvider.instance.getDatabase();

  //run the app
  runApp(
    MultiProvider(
      providers: [
        //product controller
        ChangeNotifierProvider<ProductController>(create: (_) => ProductController()),
        //stock movement controller
        ChangeNotifierProvider<StockMovementController>(create: (_) => StockMovementController()),
        //inventory loss controller
        ChangeNotifierProvider<InventoryLossController>(create: (_) => InventoryLossController()),
        //cart controller
        ChangeNotifierProvider<CartController>(create: (_) => CartController()),
        //sale controller
        ChangeNotifierProvider<ReceiptController>(create: (_) => ReceiptController()),
        //report controller
        ChangeNotifierProvider<ReportController>(create: (_) => ReportController()),
      ],
      child: const RetailApp(),
    ),
  );
}

class RetailApp extends StatelessWidget {
  const RetailApp({super.key});

  @override
  Widget build(BuildContext context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: appName,
        // theme: themeData,
        theme: ThemeData(
          useMaterial3: true,
          colorScheme: ColorScheme.fromSeed(
            seedColor: Colors.purple,
            secondary: const Color.fromRGBO(14, 18, 53, 1.0),
            brightness: Brightness.light,
            surface: const Color.fromRGBO(222, 223, 232, 1.0),
          ),
        ),
        home: const RetailHomePage(),
      );
}

class RetailHomePage extends StatefulWidget {
  const RetailHomePage({super.key});

  @override
  State<RetailHomePage> createState() => _RetailHomePageState();
}

class _RetailHomePageState extends State<RetailHomePage> {
  DateTime currentBackPressTime = DateTime.now();
  late double xOffset;
  late double yOffset;
  late double scaleFactor;
  DrawerItem item = DrawerItems.home;
  bool isDragging = false;
  bool isDrawerOpen = false;

  //initially close the drawer
  @override
  void initState() {
    super.initState();
    _closeDrawer();
  }

  void _closeDrawer() => setState(() {
        xOffset = 0;
        yOffset = 0;
        scaleFactor = 1;
        isDrawerOpen = false;
      });

  void _openDrawer() => setState(() {
        xOffset = 230;
        yOffset = 150;
        scaleFactor = 0.6;
        isDrawerOpen = true;
      });

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Theme.of(context).colorScheme.secondary,
        body: Stack(children: [
          _buildDrawer(),
          _buildPage(),
        ]),
      );

  Widget _buildDrawer() => SafeArea(
          child: SizedBox(
        width: xOffset,
        child: DrawerWidget(
          onSelectedItem: (item) {
            switch (item) {
              case DrawerItems.logout:
                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text("Logout")));
                return;
              default:
                setState(() => this.item = item);
                _closeDrawer();
            }
          },
        ),
      ));

  Widget _buildPage() => PopScope(
        canPop: true,
        onPopInvoked: (didPop) async {
          DateTime now = DateTime.now();
          if (isDrawerOpen) {
            _closeDrawer();
            if (now.difference(currentBackPressTime) > const Duration(seconds: 2)) {
              currentBackPressTime = now;
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  backgroundColor: Colors.black87,
                  content: Text(
                    'Tap Again to Exit',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              );
              // return Future.value(false);
            }
            // return false;
          } else {
            if (now.difference(currentBackPressTime) > const Duration(seconds: 2)) {
              currentBackPressTime = now;
              ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(
                  content: Text('Tap Again to Exit'),
                ),
              );
              return Future.value(false);
            }
            // return true;
          }
        },
        child: GestureDetector(
          onTap: _closeDrawer,
          onHorizontalDragStart: (details) => isDragging = true,
          onHorizontalDragUpdate: (details) {
            if (!isDragging) return;
            const delta = 1;
            if (details.delta.dx > delta) {
              _openDrawer();
            } else if (details.delta.dx < -delta) {
              _closeDrawer();
            }
            isDragging = false;
          },
          child: AnimatedContainer(
            duration: const Duration(milliseconds: 250),
            transform: Matrix4.translationValues(xOffset, yOffset, 0)..scale(scaleFactor),
            child: AbsorbPointer(
              absorbing: isDrawerOpen,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(isDrawerOpen ? 20 : 0),
                child: Container(color: isDrawerOpen ? Colors.white : Theme.of(context).primaryColor, child: _getDrawerPage()),
              ),
            ),
          ),
        ),
      );

  Widget _getDrawerPage() {
    switch (item) {
      case DrawerItems.home:
        return HomePage(openDrawer: _openDrawer);

      case DrawerItems.sales:
        return PosIndex(openDrawer: _openDrawer);

      case DrawerItems.inventory:
        return InventoryDashboard(openDrawer: _openDrawer);

      case DrawerItems.products:
        return ProductServiceScreen(openDrawer: _openDrawer);

      case DrawerItems.reports:
        return InventoryStatusReport(openDrawer: _openDrawer);

      case DrawerItems.settings:
        return Settings(openDrawer: _openDrawer);

      default:
        return HomePage(openDrawer: _openDrawer);
    }
  }
}
